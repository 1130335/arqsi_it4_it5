﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _3DRenderer.Repository
{
    public interface IRepository<T>
    {
        List<T> GetData();
        T GetData(int id);
        T Create(T obj);
        bool Update(int id, T obj);
        bool Delete(int id);
    }
}