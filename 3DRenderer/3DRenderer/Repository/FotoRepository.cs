﻿using _3DRenderer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _3DRenderer.Repository
{
    public class FotoRepository : IRepository<Foto>
    {
        public ApplicationDbContext context;

        public FotoRepository()
        {
            context = new ApplicationDbContext();


        }

        public Foto Create(Foto obj)
        {
            // Não importante no contexto da iteração 4 e 5
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            // Não importante no contexto da iteração 4 e 5
            throw new NotImplementedException();
        }

        public List<Foto> GetData()
        {
            return context.Foto.ToList();
        }

        public Foto GetData(int id)
        {
            return context.Foto.Find(id);
        }

        public bool Update(int id, Foto obj)
        {
            // Não importante no contexto da iteração 4 e 5
            throw new NotImplementedException();
        }
    }
}