﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using _3DRenderer.Models;

namespace _3DRenderer.Controllers
{
    public class FotoesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Fotoes
        public IQueryable<Foto> GetFoto()
        {
            return db.Foto;
        }

        // GET: api/Fotoes/5
        [ResponseType(typeof(Foto))]
        public async Task<IHttpActionResult> GetFoto(int id)
        {
            Foto foto = await db.Foto.FindAsync(id);
            if (foto == null)
            {
                return NotFound();
            }

            return Ok(foto);
        }

        // Irrelevante no contexto da iteração 4 e 5
        //// PUT: api/Fotoes/5
        //[ResponseType(typeof(void))]
        //public async Task<IHttpActionResult> PutFoto(int id, Foto foto)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != foto.FotoID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(foto).State = EntityState.Modified;

        //    try
        //    {
        //        await db.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!FotoExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST: api/Fotoes
        //[ResponseType(typeof(Foto))]
        //public async Task<IHttpActionResult> PostFoto(Foto foto)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Foto.Add(foto);
        //    await db.SaveChangesAsync();

        //    return CreatedAtRoute("DefaultApi", new { id = foto.FotoID }, foto);
        //}

        //// DELETE: api/Fotoes/5
        //[ResponseType(typeof(Foto))]
        //public async Task<IHttpActionResult> DeleteFoto(int id)
        //{
        //    Foto foto = await db.Foto.FindAsync(id);
        //    if (foto == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Foto.Remove(foto);
        //    await db.SaveChangesAsync();

        //    return Ok(foto);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FotoExists(int id)
        {
            return db.Foto.Count(e => e.FotoID == id) > 0;
        }
    }
}