namespace _3DRenderer.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<_3DRenderer.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(_3DRenderer.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Foto.AddOrUpdate(
                new Foto { URL = "http://images.carlsguides.com/carls-sims-3-guide/screenshots/homebuilding/intermediate/17.jpg" },
                new Foto { URL = "http://www.mysimrealty.com/Images/Screenshot-273.jpg" },
                new Foto { URL = "https://i.ytimg.com/vi/CGn6ZU4XsiM/maxresdefault.jpg" },
                new Foto { URL = "http://ts2lotmakeoverdatabase.weebly.com/uploads/5/1/7/9/5179947/1115490_orig.jpg" },
                new Foto { URL = "http://2.bp.blogspot.com/-OaHXx7tDTDY/T-QGutBgx5I/AAAAAAAAEKQ/L0GsR9KcwrE/s1600/Edif%C3%ADcio+Caiub%C3%AD.jpg" },
                new Foto { URL = "https://p.dreamwidth.org/696a3b9136a6/547150-265/i32.photobucket.com/albums/d18/LaLaLuna05/Belladonna%20City/SeptSims2ep92010-09-1800-53-15-89-201009181.jpg" },
                new Foto { URL = "http://www.spring4sims.com/wp-content/uploads/2009/01/boston01.jpg" },
                new Foto { URL = "https://p.dreamwidth.org/1111bd51d564/547150-265/i32.photobucket.com/albums/d18/LaLaLuna05/Belladonna%20City/SeptSims2ep92010-09-0914-39-40-55-201009181.jpg" }
                );
        }
    }
}
