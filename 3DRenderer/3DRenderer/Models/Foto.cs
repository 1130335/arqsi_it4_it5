﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _3DRenderer.Models
{
    public class Foto
    {
        public int FotoID { get; set; }
        [DataType(DataType.Url)]
        public String URL { get; set; }
    }
}