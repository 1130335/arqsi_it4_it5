﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APPAdministrador.ViewModels;

namespace APPAdministrador.Controllers
{
    public class AlertasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Alertas
        public async Task<ActionResult> Index()
        {
            return View(await db.Alertas.ToListAsync());
        }

        // GET: Alertas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = await db.Alertas.FindAsync(id);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            return View(alerta);
        }

        // POST: Alertas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,applicationUserID,intervaloAlerta")] Alerta alerta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(alerta).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(alerta);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
