﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APPAdministrador.ViewModels;
using APPAdministrador.Helpers;
using System.Diagnostics;
using System.IO;
using LumenWorks.Framework.IO.Csv;
using System.Net.Http;
using Newtonsoft.Json;

namespace APPAdministrador.Controllers
{
    public class FicheiroViewModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: FicheiroViewModels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FicheiroViewModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(HttpPostedFileBase upload)
        {
            //DataTable csvTable = new DataTable();
            //if (ModelState.IsValid)
            //{

            //using (CsvReader Reader =
            //    new CsvReader(new StreamReader(upload.InputStream), true))
            //{
            //    csvTable.Load(Reader);
            //}

            //LocalizaçãoViewModel LA = null;
            //LocalizaçãoViewModel LB= null;
            //DistanciaViewModel Dist;
            //foreach(DataRow row in csvTable.Rows)
            //{
            //    bool LocExists = false;
            //    foreach(var Loc in db.Localização)
            //    {
            //        if(row.Field<String>(0) == Loc.Nome)
            //        {
            //            LocExists = true;
            //            LA = Loc;
            //            break;
            //        }
            //    }

            //    if(!LocExists)
            //    {
            //        LA = new LocalizaçãoViewModel()
            //        {
            //            Nome = row.Field<String>(0)
            //        };
            //        db.Localização.Add(LA);
            //    }

            //    LocExists = false;
            //    foreach (var Loc in db.Localização)
            //    {
            //        if (row.Field<String>(1) == Loc.Nome)
            //        {
            //            LocExists = true;
            //            LB = Loc;
            //            break;
            //        }
            //    }

            //    if (!LocExists)
            //    {
            //        LB = new LocalizaçãoViewModel()
            //        {
            //            Nome = row.Field<String>(1)
            //        };
            //        db.Localização.Add(LB);
            //    }

            //    Dist = new DistanciaViewModel()
            //    {
            //        LocANome = LA.Nome,
            //        LocBNome = LB.Nome,
            //        DistanciaEmTempo = float.Parse(row.Field<String>(2))
            //    };
            //    db.Distancia.Add(Dist);
            //    db.SaveChanges();

            var client = WebApiHttpClient.GetClientSAPAgendas();
            HttpResponseMessage response = await client.GetAsync("api/Localização");
            DataTable csvTable = new DataTable();
            if (ModelState.IsValid)
            {

                using (CsvReader Reader =
                    new CsvReader(new StreamReader(upload.InputStream), true))
                {
                    csvTable.Load(Reader);
                }

                LocalizaçãoViewModel LA = null;
                LocalizaçãoViewModel LB = null;
                DistanciaViewModel Dist;
                foreach (DataRow row in csvTable.Rows)
                {
                    bool LocExists = false;
                    if (response.IsSuccessStatusCode)
                    {
                        string content1 = await response.Content.ReadAsStringAsync();
                        var LocList = JsonConvert.DeserializeObject < List < LocalizaçãoViewModel>>(content1);
                        foreach (var Loc in LocList)
                        {
                            if (row.Field<String>(0) == Loc.Nome)
                            {
                                LocExists = true;
                                LA = Loc;
                                break;
                            }
                        }
                    }

                    if (!LocExists)
                    {
                        LA = new LocalizaçãoViewModel()
                        {
                            Nome = row.Field<String>(0)
                        };
                        string LocAJSON = JsonConvert.SerializeObject(LA);
                        HttpContent content2 = new StringContent(LocAJSON,
                        System.Text.Encoding.Unicode, "application/json");
                        response = await client.PostAsync("api/Localização", content2);
                    }

                    LocExists = false;
                    if (response.IsSuccessStatusCode)
                    {
                        string content3 = await response.Content.ReadAsStringAsync();
                        var LocList = JsonConvert.DeserializeObject<List<LocalizaçãoViewModel>>(content3);
                        foreach (var Loc in LocList)
                        {
                            if (row.Field<String>(1) == Loc.Nome)
                            {
                                LocExists = true;
                                LB = Loc;
                                break;
                            }
                        }
                    }

                    if (!LocExists)
                    {
                        LB = new LocalizaçãoViewModel()
                        {
                            Nome = row.Field<String>(1)
                        };
                        string LocBJSON = JsonConvert.SerializeObject(LB);
                        HttpContent content4 = new StringContent(LocBJSON,
                        System.Text.Encoding.Unicode, "application/json");
                        response = await client.PostAsync("api/Localização", content4);
                    }

                    Dist = new DistanciaViewModel()
                    {
                        LocANome = LA.Nome,
                        LocBNome = LB.Nome,
                        DistanciaEmTempo = float.Parse(row.Field<String>(2))
                    };
                    string DistJSON = JsonConvert.SerializeObject(Dist);
                    HttpContent content5 = new StringContent(DistJSON,
                    System.Text.Encoding.Unicode, "application/json");
                    response = await client.PostAsync("api/Distancias", content5);

                }
                return RedirectToAction("Index", "Home");

            }

            return View(csvTable);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
