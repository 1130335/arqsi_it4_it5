﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APPAdministrador.ViewModels;

namespace APPAdministrador.Controllers
{
    public class Anuncio_CompraController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Anuncio_Compra
        public async Task<ActionResult> Index()
        {
            ViewBag.Anuncios_Compra = db.Anuncio_Compra.ToList();
            ViewBag.Anuncios_Venda = db.Anuncio_Venda.ToList();
            return View();
        }

        // GET: Anuncio_Compra/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Anuncio_Compra anuncio_Compra = await db.Anuncio_Compra.FindAsync(id);
            if (anuncio_Compra == null)
            {
                return HttpNotFound();
            }
            return View(anuncio_Compra);
        }

        // POST: Anuncio_Compra/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,ImovelID,applicationUserID,tipoImovelID,tipoImovel,localizacaoID,local,latitude,longitude,areaMin,areaMax,precoMin,precoMax,valido,MediadorID,AnuncioVendaID")] Anuncio_Compra anuncio_Compra)
        {
            if (ModelState.IsValid)
            {
                db.Entry(anuncio_Compra).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(anuncio_Compra);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
