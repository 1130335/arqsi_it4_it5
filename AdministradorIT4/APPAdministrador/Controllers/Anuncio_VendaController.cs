﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APPAdministrador.ViewModels;

namespace APPAdministrador.Controllers
{
    public class Anuncio_VendaController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

       
        // GET: Anuncio_Venda/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Anuncio_Venda anuncio_Venda = await db.Anuncio_Venda.FindAsync(id);
            if (anuncio_Venda == null)
            {
                return HttpNotFound();
            }
            return View(anuncio_Venda);
        }

        // POST: Anuncio_Venda/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,ImovelID,applicationUserID,preco,tipoImovelID,tipoImovel,localizacaoID,local,latitude,longitude,area,fotoID,fotoContent,fotoContentType,valido,MediadorID,AnuncioCompraID,IntervaloCorrespondencia")] Anuncio_Venda anuncio_Venda)
        {
            if (ModelState.IsValid)
            {
                db.Entry(anuncio_Venda).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index", "Home");
            }
            return View(anuncio_Venda);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
