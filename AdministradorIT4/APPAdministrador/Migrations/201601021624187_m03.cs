namespace APPAdministrador.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m03 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Anuncio_Aluguer",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ImovelID = c.Int(nullable: false),
                        applicationUserID = c.String(),
                        renda = c.Single(nullable: false),
                        tipoImovelID = c.Int(nullable: false),
                        tipoImovel = c.String(),
                        localizacaoID = c.Int(nullable: false),
                        local = c.String(),
                        latitude = c.String(),
                        longitude = c.String(),
                        area = c.Single(nullable: false),
                        fotoID = c.Int(nullable: false),
                        fotoContent = c.String(),
                        fotoContentType = c.String(),
                        valido = c.String(),
                        MediadorID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Anuncio_Compra",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ImovelID = c.Int(nullable: false),
                        applicationUserID = c.String(),
                        tipoImovelID = c.Int(nullable: false),
                        tipoImovel = c.String(),
                        localizacaoID = c.Int(nullable: false),
                        local = c.String(),
                        latitude = c.String(),
                        longitude = c.String(),
                        areaMin = c.Single(nullable: false),
                        areaMax = c.Single(nullable: false),
                        precoMin = c.Single(nullable: false),
                        precoMax = c.Single(nullable: false),
                        valido = c.String(),
                        MediadorID = c.String(),
                        AnuncioVendaID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Anuncio_Permuta",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ImovelID = c.Int(nullable: false),
                        applicationUserID = c.String(),
                        preco = c.Single(nullable: false),
                        tipoImovelID = c.Int(nullable: false),
                        tipoImovel = c.String(),
                        localizacaoID = c.Int(nullable: false),
                        local = c.String(),
                        latitude = c.String(),
                        longitude = c.String(),
                        area = c.Single(nullable: false),
                        fotoID = c.Int(nullable: false),
                        fotoContent = c.String(),
                        fotoContentType = c.String(),
                        valido = c.String(),
                        MediadorID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Anuncio_Venda",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ImovelID = c.Int(nullable: false),
                        applicationUserID = c.String(),
                        preco = c.Single(nullable: false),
                        tipoImovelID = c.Int(nullable: false),
                        tipoImovel = c.String(),
                        localizacaoID = c.Int(nullable: false),
                        local = c.String(),
                        latitude = c.String(),
                        longitude = c.String(),
                        area = c.Single(nullable: false),
                        fotoID = c.Int(nullable: false),
                        fotoContent = c.String(),
                        fotoContentType = c.String(),
                        valido = c.String(),
                        MediadorID = c.String(),
                        AnuncioCompraID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Fotoes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        ContentType = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Imovel_Compra",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        applicationUserID = c.String(),
                        tipoImovelID = c.Int(nullable: false),
                        tipoImovel = c.String(),
                        localizacaoID = c.Int(nullable: false),
                        local = c.String(),
                        latitude = c.String(),
                        longitude = c.String(),
                        areaMin = c.Single(nullable: false),
                        areaMax = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Imovel_Venda_Permuta_Aluguer",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        applicationUserID = c.String(),
                        tipoImovelID = c.Int(nullable: false),
                        tipoImovel = c.String(),
                        localizacaoID = c.Int(nullable: false),
                        local = c.String(),
                        latitude = c.String(),
                        longitude = c.String(),
                        area = c.Single(nullable: false),
                        fotoID = c.Int(nullable: false),
                        fotoContent = c.String(),
                        fotoContentType = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ParametroFixo",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        valor_string = c.String(),
                        nome = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TipoImovels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        nome = c.String(),
                        subtipo_id = c.Int(nullable: false),
                        subtipo = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TipoImovels");
            DropTable("dbo.ParametroFixo");
            DropTable("dbo.Imovel_Venda_Permuta_Aluguer");
            DropTable("dbo.Imovel_Compra");
            DropTable("dbo.Fotoes");
            DropTable("dbo.Anuncio_Venda");
            DropTable("dbo.Anuncio_Permuta");
            DropTable("dbo.Anuncio_Compra");
            DropTable("dbo.Anuncio_Aluguer");
        }
    }
}
