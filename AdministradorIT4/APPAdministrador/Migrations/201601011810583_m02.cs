namespace APPAdministrador.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m02 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.DistanciaViewModels", "LocAID");
            DropColumn("dbo.DistanciaViewModels", "LocBID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DistanciaViewModels", "LocBID", c => c.Int(nullable: false));
            AddColumn("dbo.DistanciaViewModels", "LocAID", c => c.Int(nullable: false));
        }
    }
}
