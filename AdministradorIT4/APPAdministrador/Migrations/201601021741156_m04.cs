namespace APPAdministrador.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m04 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Anuncio_Compra", "IntervaloCorrespondencia", c => c.Single(nullable: false));
            AddColumn("dbo.Anuncio_Venda", "IntervaloCorrespondencia", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Anuncio_Venda", "IntervaloCorrespondencia");
            DropColumn("dbo.Anuncio_Compra", "IntervaloCorrespondencia");
        }
    }
}
