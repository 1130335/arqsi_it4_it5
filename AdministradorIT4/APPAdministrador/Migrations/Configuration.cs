namespace APPAdministrador.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<APPAdministrador.ViewModels.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(APPAdministrador.ViewModels.ApplicationDbContext context)
        {
            context.Alertas.AddOrUpdate(
                new ViewModels.Alerta
                {
                    ID = 1,
                },

                new ViewModels.Alerta
                {
                    ID = 2,
                },

                new ViewModels.Alerta
                {
                    ID = 3,
                }

                );

            context.Anuncio_Venda.AddOrUpdate(
                new ViewModels.Anuncio_Venda
                {
                    ID = 1,
                    local = "Porto",
                    tipoImovel = "T3"
                },

                new ViewModels.Anuncio_Venda
                {
                    ID = 2,
                    local = "Braga",
                    tipoImovel = "T1"
                },

                new ViewModels.Anuncio_Venda
                {
                    ID = 3,
                    local = "Lisboa",
                    tipoImovel = "Moradia"
                }
                );
        }
    }
}
