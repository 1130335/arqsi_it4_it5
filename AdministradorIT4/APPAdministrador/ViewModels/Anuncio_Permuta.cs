﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace APPAdministrador.ViewModels
{
    public class Anuncio_Permuta
    {
        public int ID { get; set; }
        public int ImovelID { get; set; }
        public string applicationUserID { get; set; }

        [DisplayName("Preço")]
        public float preco { get; set; }
        public int tipoImovelID { get; set; }
        public string tipoImovel { get; set; }
        public int localizacaoID { get; set; }
        public string local { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public float area { get; set; }
        public int fotoID { get; set; }
        public string fotoContent { get; set; }
        public string fotoContentType { get; set; }
        public string valido { get; set; }
        public string MediadorID { get; set; }
    }
}