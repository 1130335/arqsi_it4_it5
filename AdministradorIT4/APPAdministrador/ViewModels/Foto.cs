﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APPAdministrador.ViewModels
{
    public class Foto
    {
        public int ID { get; set; }
        public string Content { get; set; }
        public string ContentType { get; set; }
    }
}