﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace APPAdministrador.ViewModels
{
    public class LocalizaçãoViewModel
    {
        [Key]
        [JsonProperty("LocalizaçãoID")]
        public int LocalizaçãoID { get; set; }
        [JsonProperty("Nome")]
        public String Nome { get; set; }
    }

    public class DistanciaViewModel
    {
        [Key]
        public int DistanciaID { get; set; }
        public String LocANome { get; set; }
        public String LocBNome { get; set; }
        public float DistanciaEmTempo { get; set; }
    }
}