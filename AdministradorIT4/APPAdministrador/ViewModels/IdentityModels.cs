﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace APPAdministrador.ViewModels
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {

        }

        public static ApplicationDbContext Create()
        {



            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<APPAdministrador.ViewModels.Alerta> Alertas { get; set; }

        public DbSet<LocalizaçãoViewModel> Localização { get; set; }

        public DbSet<DistanciaViewModel> Distancia { get; set; }

        public System.Data.Entity.DbSet<TipoImovel> TipoImovels { get; set; }

        public System.Data.Entity.DbSet<Imovel_Compra> Imovel_Compra { get; set; }

        public System.Data.Entity.DbSet<Anuncio_Compra> Anuncio_Compra { get; set; }

        public System.Data.Entity.DbSet<Imovel_Venda_Permuta_Aluguer> Imovel_Venda_Permuta_Aluguer { get; set; }

        public System.Data.Entity.DbSet<Anuncio_Venda> Anuncio_Venda { get; set; }

        public System.Data.Entity.DbSet<Anuncio_Permuta> Anuncio_Permuta { get; set; }

        public System.Data.Entity.DbSet<Anuncio_Aluguer> Anuncio_Aluguer { get; set; }

        public System.Data.Entity.DbSet<ParametroFixo> ParametroFixoes { get; set; }

        public System.Data.Entity.DbSet<Foto> Fotoes { get; set; }

    }
}