﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace APPAdministrador.ViewModels
{
    public class Anuncio_Compra
    {
        public int ID { get; set; }
        public int ImovelID { get; set; }
        public string applicationUserID { get; set; }
        public int tipoImovelID { get; set; }

        [DisplayName("Tipo de Imóvel")]
        public string tipoImovel { get; set; }

        public int localizacaoID { get; set; }

        [DisplayName("Local")]
        public string local { get; set; }

        [DisplayName("Latitude")]
        public string latitude { get; set; }

        [DisplayName("Longitude")]
        public string longitude { get; set; }

        [DisplayName("Área Mínima")]
        public float areaMin { get; set; }

        [DisplayName("Área Máxima")]
        public float areaMax { get; set; }

        [DisplayName("Preço Mínimo")]
        public float precoMin { get; set; }

        [DisplayName("Preço Máximo")]
        public float precoMax { get; set; }

        public string valido { get; set; }
        public string MediadorID { get; set; }
        public int AnuncioVendaID { get; set; }
        public float IntervaloCorrespondencia { get; set; }
    }
}