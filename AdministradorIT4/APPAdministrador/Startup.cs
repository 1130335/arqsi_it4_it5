﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(APPAdministrador.Startup))]
namespace APPAdministrador
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
