﻿using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClassLibrary.DTO
{
    public class TipoImovelDTO
    {
        public int ID { get; set; }
        public string nome { get; set; }
        public int subtipo_id { get; set; }
        public string subtipo { get; set; }

        public TipoImovelDTO(TipoImovel t)
        {
            if(t==null)
            {
                return;
            }
            ID = t.ID;
            nome = t.nome;
            if (t.subTipo != null)
            {
                subtipo_id = t.subTipo.ID;
                subtipo = t.subTipo.nome;
            }
        }
    }
}