﻿using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DTO
{
    public class VisitaDTO
    {
        public int ID { get; set; }

        public int ImovelID { get; set; }
        public string tipoImovel { get; set; }

        public float HoraInicio { get; set; }
        public float HoraFim { get; set; }
        public int Dia { get; set; }
        public int Mes { get; set; }
        public int Ano { get; set; }

        public VisitaDTO()
        {

        }

        public VisitaDTO(Visita t)
        {
            if (t == null)
            {
                return;
            }

            ID = t.ID;
            ImovelID = t.imovel.ID;
            tipoImovel = t.imovel.tipo_de_imovel.nome;
            HoraInicio = t.HoraInicio;
            HoraFim = t.HoraFim;
            Dia = t.Dia;
            Mes = t.Mes;
            Ano = t.Ano;
        }
    }
}
