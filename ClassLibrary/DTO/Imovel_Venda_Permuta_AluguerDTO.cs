﻿using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DTO
{
    public class Imovel_Venda_Permuta_AluguerDTO
    {
        public int ID { get; set; }
        public string applicationUserID { get; set; }
        public int tipoImovelID { get; set; }
        public string tipoImovel { get; set; }
        public int localizacaoID { get; set; }
        public string local { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public float area { get; set; }
        public int fotoID { get; set; }
        public string fotoContent { get; set; }
        public string fotoContentType { get; set; }
        
        public Imovel_Venda_Permuta_AluguerDTO ()
        {

        }
        public Imovel_Venda_Permuta_AluguerDTO (Imovel_Venda_Permuta_Aluguer imovel)
        {
            ID = imovel.ID;
            applicationUserID = imovel.ApplicationUserID;
            tipoImovelID = imovel.TipoImovelID;
            tipoImovel = imovel.tipo_de_imovel.nome;
            localizacaoID = imovel.localizacaoID;
            local = imovel.localizacao.local;
            latitude = imovel.localizacao.latitude;
            longitude = imovel.localizacao.longitude;
            area = imovel.area;
            if(imovel.Fotos.Count != 0)
            {
                fotoContent = System.Text.Encoding.UTF8.GetString(imovel.Fotos.First().Content, 0, imovel.Fotos.First().Content.Length);
                fotoContentType = imovel.Fotos.First().ContentType;
                fotoID = imovel.Fotos.First().ID;
            }

            
        }
    }
}
