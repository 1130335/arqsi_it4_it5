﻿using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DTO
{
    public class FotoDTO
    {
        public int ID { get; set; }
        public string Content { get; set; }
        public string ContentType { get; set; }
        
        public FotoDTO()
        {

        }

        public FotoDTO (Foto f)
        {
            ID = f.ID;
            Content = System.Text.Encoding.Default.GetString(f.Content);
            ContentType = f.ContentType;
        }

    }
}
