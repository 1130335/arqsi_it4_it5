﻿using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DTO
{
    public class AlertaDTO
    {
        public int ID { get; set; }
        public string applicationUserID { get; set; }
        public float intervaloAlerta { get; set; }

        public List<List<String>> fixo { get; set; }
        public List<List<String>> parametroAlfaNumerico { get; set; }
        public List<List<String>> parametroIntervaloNumerico { get; set; }

        public AlertaDTO()
        {

        }

        public AlertaDTO(Alerta a)
        {
            ID = a.ID;
            applicationUserID = a.ApplicationUserID;
            intervaloAlerta = a.intervaloAlerta;
            fixo = new List<List<string>>();
            foreach(ParametroFixo p in a.fixo)
            {
                List<string> tmp = new List<string>();

                tmp.Add(p.ID.ToString());
                tmp.Add(p.nome.ToString());
                tmp.Add(p.valor_string);

                fixo.Add(tmp);
            }

            parametroAlfaNumerico = new List<List<string>>();
            foreach (ParametroConstantesAlfaNumerico p in a.parametroAlfaNumerico)
            {
                List<string> tmp = new List<string>();

                tmp.Add(p.ID.ToString());
                tmp.Add(p.nome.ToString());
                tmp.Add(p.str);

                parametroAlfaNumerico.Add(tmp);
            }

            parametroIntervaloNumerico = new List<List<string>>();
            foreach (ParametroConstantesIntervaloNumerico p in a.parametroIntervaloNumerico)
            {
                List<string> tmp = new List<string>();

                tmp.Add(p.ID.ToString());
                tmp.Add(p.nome.ToString());
                tmp.Add(p.min.ToString());
                tmp.Add(p.max.ToString());

                parametroIntervaloNumerico.Add(tmp);
            }
        }
        
    }
}
