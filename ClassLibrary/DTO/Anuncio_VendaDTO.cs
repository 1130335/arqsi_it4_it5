﻿using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DTO
{
    public class Anuncio_VendaDTO
    {
        public int ID { get; set; }
        public int ImovelID { get; set; }
        public string applicationUserID { get; set; }
        public float preco { get; set; }
        public int tipoImovelID { get; set; }
        public string tipoImovel { get; set; }
        public int localizacaoID { get; set; }
        public string local { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public float area { get; set; }
        public int fotoID { get; set; }
        public string fotoContent { get; set; }
        public string fotoContentType { get; set; }
        public string valido { get; set; }
        public string MediadorID { get; set; }
        public int AnuncioCompraID { get; set; }

        public Anuncio_VendaDTO()
        {

        }

        public Anuncio_VendaDTO(Anuncio_Venda a,Imovel_Venda_Permuta_Aluguer imovel)
        {
            preco = a.preco;
            ImovelID = a.ImovelID;
            ID = a.ID;
            applicationUserID = a.ApplicationUserID;
            tipoImovelID = imovel.TipoImovelID;
            tipoImovel = imovel.tipo_de_imovel.nome;
            localizacaoID = imovel.localizacaoID;
            local = imovel.localizacao.local;
            latitude = imovel.localizacao.latitude;
            longitude = imovel.localizacao.longitude;
            area = imovel.area;
            valido = a.valido;
            MediadorID = a.MediadorID;
            AnuncioCompraID = a.AnuncioCompraID;
            if (imovel.Fotos.Count != 0)
            {
                fotoContent = System.Text.Encoding.UTF8.GetString(imovel.Fotos.First().Content, 0, imovel.Fotos.First().Content.Length);
                fotoContentType = imovel.Fotos.First().ContentType;
                fotoID = imovel.Fotos.First().ID;
            }
        }
    }
}
