﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClassLibrary.Models
{
    public abstract class Imovel
    {

        public int ID { get; set; }

        [Required(ErrorMessage = "Escolha um tipo de imóvel"), HiddenInput(DisplayValue = false)]
        public int TipoImovelID { get; set; }

        [DisplayName("Tipo de Imóvel")]
        public virtual TipoImovel tipo_de_imovel { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int localizacaoID { get; set; }

        [DisplayName("Localização GPS")]
        public virtual Localizacao localizacao { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ApplicationUserID { get; set; }
        [HiddenInput(DisplayValue = false)]
        public virtual ApplicationUser user { get; set; }
    }
    [Table("Imovel_Venda_Permuta_Aluguer")]
    public class Imovel_Venda_Permuta_Aluguer : Imovel 
    {
        [Range(0, float.MaxValue, ErrorMessage = "Insira um valor válido")]
        [DisplayName("Área (m²)")]
        public float area { get; set; }

        [DisplayName("Fotos")]
        public virtual ICollection<Foto> Fotos { get; set; }
    }

    [Table("Imovel_Compra")]
    public class Imovel_Compra : Imovel
    {
        [Range(0, float.MaxValue, ErrorMessage = "Insira um valor válido")]
        [DisplayName("Área Máxima (m²)")]
        public float areaMAx { get; set; }

        [Range(0, float.MaxValue, ErrorMessage = "Insira um valor válido")]
        [DisplayName("Área Mínima (m²)")]
        public float areaMin { get; set; }
    }
}