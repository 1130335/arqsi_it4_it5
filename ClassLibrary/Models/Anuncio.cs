﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClassLibrary.Models
{
    public abstract class Anuncio
    {
        [HiddenInput(DisplayValue = false)]
        public int ID { get; set; }

        public string ApplicationUserID { get; set; }

        public virtual ApplicationUser user { get; set; }

        public string valido { get; set; }

        public string MediadorID { get; set; }
        
    }
    [Table("Anuncio_Compra")]
    public class Anuncio_Compra : Anuncio
    {
        [Required, Range(0, float.MaxValue, ErrorMessage = "Insira um valor válido")]
        [DisplayName("Preço Máximo (€) ")]
        public float precoMax { get; set; }

        [Required, Range(0, float.MaxValue, ErrorMessage = "Insira um valor válido")]
        [DisplayName("Preço Mínimo (€) ")]
        public float precoMin { get; set; }

        [Required(ErrorMessage = "Escolha um imóvel"), HiddenInput(DisplayValue = false)]
        public int ImovelID { get; set; }

        public int AnuncioVendaID { get;set; }

        [DisplayName("Imóvel")]
        public virtual Imovel_Compra Imovel { get; set; }
    }

    [Table("Anuncio_Permuta")]
    public class Anuncio_Permuta : Anuncio
    {
        [Required, Range(0, float.MaxValue, ErrorMessage = "Insira um valor válido")]
        [DisplayName("Preço (€) ")]
        public float preco { get; set; }

        [Required(ErrorMessage = "Escolha um imóvel"), HiddenInput(DisplayValue = false)]
        public int ImovelID { get; set; }

        [DisplayName("Imóvel")]
        public virtual Imovel_Venda_Permuta_Aluguer Imovel { get; set; }
    }

    [Table("Anuncio_Aluguer")]
    public class Anuncio_Aluguer : Anuncio
    {
        [Required, Range(0, float.MaxValue, ErrorMessage = "Insira um valor válido")]
        [DisplayName("Renda (€) ")]
        public float renda { get; set; }

        [Required(ErrorMessage = "Escolha um imóvel"), HiddenInput(DisplayValue = false)]
        public int ImovelID { get; set; }

        [DisplayName("Imóvel")]
        public virtual Imovel_Venda_Permuta_Aluguer Imovel { get; set; }
    }

    [Table("Anuncio_Venda")]
    public class Anuncio_Venda : Anuncio
    {
        [Required, Range(0, float.MaxValue, ErrorMessage = "Insira um valor válido")]
        [DisplayName("Preço (€) ")]
        public float preco { get; set; }

        [Required(ErrorMessage = "Escolha um imóvel"), HiddenInput(DisplayValue = false)]
        public int ImovelID { get; set; }

        public int AnuncioCompraID { get; set; }

        [DisplayName("Imóvel")]
        public virtual Imovel_Venda_Permuta_Aluguer Imovel { get; set; }
    }
}