﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClassLibrary.Models
{
    public class Alerta
    {

        public int ID { get; set; }
        public float intervaloAlerta { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ApplicationUserID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public virtual ApplicationUser user { get; set; }

        public virtual ICollection<ParametroFixo> fixo { get; set; }
        
        public virtual ICollection<ParametroConstantesAlfaNumerico>  parametroAlfaNumerico { get; set; }

        public virtual ICollection<ParametroConstantesIntervaloNumerico>  parametroIntervaloNumerico { get; set; }
    }
}