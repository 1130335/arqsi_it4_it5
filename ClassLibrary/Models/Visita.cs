﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ClassLibrary.Models
{
    public class Visita
    {
        public int ID { get; set; }

        public int imovelID { get; set; }
        public virtual Imovel imovel { get; set; }
        public virtual TipoImovel tpimovel { get; set; }

        public float HoraInicio { get; set; }
        public float HoraFim { get; set; }

        [Required, Range(1, 31, ErrorMessage = "Insira um valor válido (1 a 31)"), DisplayName("Dia:")]
        public int Dia { get; set; }
        [Required, Range(1, 12, ErrorMessage = "Insira um valor válido (1 a 12)"), DisplayName("Mês:")]
        public int Mes { get; set; }
        [Required, Range(2016, 2050, ErrorMessage = "Insira um valor válido (2016 a 2050)"), DisplayName("Ano:")]
        public int Ano { get; set; }

    }
}
