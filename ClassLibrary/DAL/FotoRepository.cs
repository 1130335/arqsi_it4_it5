﻿using ClassLibrary.DTO;
using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DAL
{
    public class FotoRepository : IRepository<FotoDTO>
    {
        public ImoveisDbContext context;

        public FotoRepository()
        {
            context = new ImoveisDbContext();

            
        }

        public FotoDTO Create(FotoDTO tipo)
        {
            Foto f = new Foto();
            f.Content = System.Text.Encoding.UTF8.GetBytes(tipo.Content);
            f.ContentType = tipo.ContentType;

            context.Fotos.Add(f);
            context.SaveChanges();
            return tipo;
        }

        public bool Delete(int id)
        {
            var f = context.Fotos.Find(id);
            if (f != null)
            {
                context.Fotos.Remove(f);
                context.SaveChanges();
                return true;
            }
            else return false;
        }

        public List<FotoDTO> GetData()
        {
            

            List<Foto> fotos = context.Fotos.ToList();
            List<FotoDTO> dto = new List<FotoDTO>();
            foreach(Foto  f in fotos)
            {
                FotoDTO tmp = new FotoDTO(f);
                dto.Add(tmp);
                
            }
            return dto;
        }

        public FotoDTO GetData(int id)
        {
            return new FotoDTO(context.Fotos.Find(id));
        }

        public bool Update(int id, FotoDTO t)
        {
            var f = context.Fotos.Find(id);
            if (t != null)
            {
                f.Content = System.Text.Encoding.UTF8.GetBytes(t.Content);
                f.ContentType = t.ContentType;
                context.SaveChanges();
                return true;
            }
            else return false;
        }
    }
}
