﻿using ClassLibrary.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ClassLibrary.DAL
{
    public class ImoveisDbContext : IdentityDbContext<ApplicationUser>
    {
        public ImoveisDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            //this.Configuration.ProxyCreationEnabled = false;
        }


        static ImoveisDbContext()
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            
            Database.SetInitializer<ImoveisDbContext>(new ImoveisInitializer());
        }

        public static ImoveisDbContext Create()
        {
            return new ImoveisDbContext();
        }

        public System.Data.Entity.DbSet<ClassLibrary.Models.Anuncio> Anuncios { get; set; }

        public System.Data.Entity.DbSet<ClassLibrary.Models.Anuncio_Aluguer> Anuncio_Aluguer { get; set; }

        public System.Data.Entity.DbSet<ClassLibrary.Models.Imovel_Venda_Permuta_Aluguer> Imovel_Venda_Permuta_Aluguer { get; set; }

        public System.Data.Entity.DbSet<ClassLibrary.Models.Anuncio_Compra> Anuncio_Compra { get; set; }

        public System.Data.Entity.DbSet<ClassLibrary.Models.Imovel_Compra> Imovel_Compra { get; set; }

        public System.Data.Entity.DbSet<ClassLibrary.Models.Anuncio_Permuta> Anuncio_Permuta { get; set; }

        public System.Data.Entity.DbSet<ClassLibrary.Models.Anuncio_Venda> Anuncio_Venda { get; set; }

        public System.Data.Entity.DbSet<ClassLibrary.Models.Imovel> Imovels { get; set; }

        public System.Data.Entity.DbSet<ClassLibrary.Models.Localizacao> Localizacaos { get; set; }

        public System.Data.Entity.DbSet<ClassLibrary.Models.TipoImovel> TipoImovels { get; set; }

        public System.Data.Entity.DbSet<ClassLibrary.Models.Foto> Fotos { get; set; }
        
        public System.Data.Entity.DbSet<ClassLibrary.Models.Alerta> Alertas { get; set; }

        public System.Data.Entity.DbSet<ClassLibrary.Models.Parametro> Parametroes { get; set; }

        public System.Data.Entity.DbSet<ClassLibrary.Models.Visita> Visitas { get; set; }

        public System.Data.Entity.DbSet<ParametroConstantesAlfaNumerico> ParametroConstantesAlfaNumericoes { get; set; }

        public System.Data.Entity.DbSet<ParametroConstantesIntervaloNumerico> ParametroConstantesIntervaloNumericoes { get; set; }

        public System.Data.Entity.DbSet<ParametroFixo> ParametroFixoes { get; set; }

    }
}