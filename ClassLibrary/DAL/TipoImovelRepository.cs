﻿using ClassLibrary.DTO;
using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DAL
{
    public class TipoImovelRepository : IRepository<TipoImovelDTO>
    {
        public ImoveisDbContext context;

        public TipoImovelRepository()
        {
            context = new ImoveisDbContext();

            
        }

        public TipoImovelDTO Create(TipoImovelDTO tipo)
        {
            TipoImovel t = new TipoImovel();
            t.nome = tipo.nome;
            if (tipo.subtipo_id != 0)
            {
                t.subTipo = new TipoImovel();
                t.subTipo.ID = tipo.subtipo_id;

                TipoImovel tmp = context.TipoImovels.Find(tipo.subtipo_id);
                t.subTipo = tmp;

            }
            context.TipoImovels.Add(t);
            context.SaveChanges();
            return tipo;
        }

        public bool Delete(int id)
        {
            var tipo = context.TipoImovels.Find(id);

            if (tipo != null)
            {
                bool flag = false;

                foreach (Imovel_Venda_Permuta_Aluguer i in context.Imovel_Venda_Permuta_Aluguer) 
                {
                    if (i.TipoImovelID == id)
                    {
                        flag = true;
                    }
                }
                foreach (Imovel_Compra i in context.Imovel_Compra)
                {
                    if (i.TipoImovelID == id)
                    {
                        flag = true;
                    }
                }

                foreach (ParametroFixo i in context.ParametroFixoes)
                {
                    if (i.nome == "TipoImovel")
                    {
                        if(i.valor_string==context.TipoImovels.Find(id).nome)
                        {
                            flag = true;
                        }
                    }
                }
                foreach (TipoImovel i in context.TipoImovels.ToList())
                {
                    if (i.subTipo != null && i.subTipo.ID == id)
                    {
                        flag = true;
                    }
                }
                if(flag==true)
                {
                    return false;
                }
                context.TipoImovels.Remove(tipo);
                context.SaveChanges();
                return true;
            }

            else return false;
        }

        public List<TipoImovelDTO> GetData()
        {
            

            List<TipoImovel> tipo = context.TipoImovels.ToList();
            List<TipoImovelDTO> dto = new List<TipoImovelDTO>();
            foreach(TipoImovel  t in tipo)
            {
                TipoImovelDTO tmp = new TipoImovelDTO(t);
                dto.Add(tmp);
                
            }
            return dto;
        }

        public TipoImovelDTO GetData(int id)
        {
            return new TipoImovelDTO(context.TipoImovels.Find(id));
        }

        public bool Update(int id, TipoImovelDTO t)
        {
            var tipo = context.TipoImovels.Find(id);
            if (t != null)
            {
                tipo.nome = t.nome;
                if(t.subtipo_id==0)
                {
                    tipo.subTipo = null;
                } else
                {
                    tipo.subTipo = context.TipoImovels.Find(t.subtipo_id);
                }
                context.SaveChanges();
                return true;
            }
            else return false;
        }


    }
}
