﻿using ClassLibrary.DTO;
using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ClassLibrary.DAL
{
    public class Anuncio_PermutaRepository : IRepository<Anuncio_PermutaDTO>
    {
        
        public ImoveisDbContext context;

        public Anuncio_PermutaRepository()
        {
            context = new ImoveisDbContext();          
        }

        public Anuncio_PermutaDTO Create(Anuncio_PermutaDTO anuncio)
        {
            Anuncio_Permuta t = new Anuncio_Permuta();
            t.preco = anuncio.preco;
            t.ImovelID = anuncio.ImovelID;
            t.ApplicationUserID = anuncio.applicationUserID;
            t.Imovel = context.Imovel_Venda_Permuta_Aluguer.Find(t.ImovelID);
            t.valido = "false";

            context.Anuncio_Permuta.Add(t);
            context.SaveChanges();
            return anuncio;
        }

        public bool Delete(int id)
        {
            var anuncio = context.Anuncio_Permuta.Find(id);
            if (anuncio != null)
            {
                context.Anuncio_Permuta.Remove(anuncio);
                context.SaveChanges();
                return true;
            }
            else return false;
        }

        public List<Anuncio_PermutaDTO> GetData()
        {
            List<Anuncio_Permuta> anuncio = context.Anuncio_Permuta.ToList();
            List<Anuncio_PermutaDTO> dto = new List<Anuncio_PermutaDTO>();
            foreach (Anuncio_Permuta t in anuncio)
            {
                Imovel_Venda_Permuta_Aluguer i = context.Imovel_Venda_Permuta_Aluguer.Find(t.ImovelID);
                Anuncio_PermutaDTO tmp = new Anuncio_PermutaDTO(t,i);
                dto.Add(tmp);
                
            }
            return dto;
        }

        public Anuncio_PermutaDTO GetData(int id)
        {
            Anuncio_Permuta a = context.Anuncio_Permuta.Find(id);
            Imovel_Venda_Permuta_Aluguer i = context.Imovel_Venda_Permuta_Aluguer.Find(a.ImovelID);
            return new Anuncio_PermutaDTO(a,i);
        }

        public bool Update(int id, Anuncio_PermutaDTO a)
        {
            var anuncio = context.Anuncio_Permuta.Find(id);
            if (a != null)
            {
                anuncio.preco = a.preco;
                anuncio.ImovelID = a.ImovelID;
                anuncio.Imovel = context.Imovel_Venda_Permuta_Aluguer.Find(a.ImovelID);
                anuncio.valido = a.valido;
                anuncio.MediadorID = a.MediadorID;
                anuncio.valido = a.valido;
                if (a.tipoImovelID != 0)
                {
                    var imovel = context.Imovel_Venda_Permuta_Aluguer.Find(anuncio.ImovelID);

                    Localizacao l = context.Localizacaos.Find(imovel.localizacaoID);
                    l.local = a.local;
                    l.longitude = a.longitude;
                    l.latitude = a.latitude;
                }

                context.SaveChanges();
                return true;
            }
            else return false;
        }
    }
}
