﻿using ClassLibrary.DTO;
using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DAL
{
    public class Imovel_CompraRepository : IRepository<Imovel_CompraDTO>
    {
        public ImoveisDbContext context;

        public Imovel_CompraRepository()
        {
            context = new ImoveisDbContext();
        }

        public Imovel_CompraDTO Create(Imovel_CompraDTO imovel)
        {
            Imovel_Compra tmp = new Imovel_Compra();
            tmp.TipoImovelID = imovel.tipoImovelID;
            tmp.tipo_de_imovel = context.TipoImovels.Find(imovel.tipoImovelID);

            Localizacao l = new Localizacao() { latitude = imovel.latitude, local = imovel.local, longitude = imovel.longitude };
            tmp.localizacao = l;

            tmp.areaMin = imovel.areaMin;
            tmp.areaMAx = imovel.areaMax;

            context.Imovel_Compra.Add(tmp);
            context.SaveChanges();
            return imovel;
        }

        public bool Delete(int id)
        {
            var imovel = context.Imovel_Compra.Find(id);
            if (imovel != null)
            {
                context.Imovel_Compra.Remove(imovel);
                context.SaveChanges();
                return true;
            }
            else return false;
        }

        public List<Imovel_CompraDTO> GetData()
        {
            List<Imovel_Compra> imoveis = context.Imovel_Compra.ToList();
            List<Imovel_CompraDTO> dto = new List<Imovel_CompraDTO>();
            foreach (Imovel_Compra tmp in imoveis)
            {
                Imovel_CompraDTO imovel = new Imovel_CompraDTO(tmp);
                dto.Add(imovel);
            }
            return dto;
        }

        public Imovel_CompraDTO GetData(int id)
        {
            return new Imovel_CompraDTO(context.Imovel_Compra.Find(id));
        }

        public bool Update(int id, Imovel_CompraDTO t)
        {
            var tipo = context.Imovel_Compra.Find(id);
            if (t != null)
            {
                context.SaveChanges();
                return true;
            }
            else return false;
        }
    }
    }
