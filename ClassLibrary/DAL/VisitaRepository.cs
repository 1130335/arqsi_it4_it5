﻿using ClassLibrary.DTO;
using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DAL
{
    public class VisitaRepository : IRepository<VisitaDTO>
    {
        public ImoveisDbContext context;

        public VisitaRepository()
        {
            context = new ImoveisDbContext();
        }

        public VisitaDTO Create(VisitaDTO visita)
        {
            Visita v = new Visita();
            v.ID = visita.ID;
            v.imovel = context.Imovels.Find(v.imovelID);
            v.tpimovel = context.TipoImovels.Find(v.imovelID);
            v.HoraInicio = visita.HoraInicio;
            v.HoraFim = visita.HoraFim;
            v.Dia = visita.Dia;
            v.Mes = visita.Mes;
            v.Ano = visita.Ano;

            context.Visitas.Add(v);
            context.SaveChanges();
            return visita;
        }

        public bool Update(int id, VisitaDTO vis)
        {
            var v = context.Visitas.Find(id);
            if (vis != null)
            {
                v.ID = vis.ID;
                v.imovel = context.Imovels.Find(v.imovelID);
                v.tpimovel = context.TipoImovels.Find(v.imovelID);
                v.HoraInicio = vis.HoraInicio;
                v.HoraFim = vis.HoraFim;
                v.Dia = vis.Dia;
                v.Mes = vis.Mes;
                v.Ano = vis.Ano;
                context.SaveChanges();
                return true;
            }
            else return false;
        }

        public bool Delete(int id)
        {
            var visita = context.Visitas.Find(id);
            if (visita != null)
            {
                context.Visitas.Remove(visita);
                context.SaveChanges();
                return true;
            }
            else return false;
        }

        public List<VisitaDTO> GetData()
        {

            List<Visita> visitas = context.Visitas.ToList();
            List<VisitaDTO> dto = new List<VisitaDTO>();

            foreach (Visita v in visitas)
            {
                VisitaDTO tmp = new VisitaDTO(v);

            }
            return dto;
        }

        public VisitaDTO GetData(int id)
        {
            return new VisitaDTO(context.Visitas.Find(id));
        }


    }

}