﻿using ClassLibrary.DTO;
using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DAL
{
    public class Anuncio_VendaRepository : IRepository<Anuncio_VendaDTO>
    {
        
        public ImoveisDbContext context;

        public Anuncio_VendaRepository()
        {
            context = new ImoveisDbContext();          
        }

        public Anuncio_VendaDTO Create(Anuncio_VendaDTO anuncio)
        {
            Anuncio_Venda t = new Anuncio_Venda();
            t.preco = anuncio.preco;
            t.ImovelID = anuncio.ImovelID;
            t.ApplicationUserID = anuncio.applicationUserID;
            t.Imovel = context.Imovel_Venda_Permuta_Aluguer.Find(t.ImovelID);
            t.valido = "false";
            t.AnuncioCompraID = anuncio.AnuncioCompraID;

            context.Anuncio_Venda.Add(t);
            context.SaveChanges();
            return anuncio;
        }

        public bool Delete(int id)
        {
            var anuncio = context.Anuncio_Venda.Find(id);
            if (anuncio != null)
            {
                context.Anuncio_Venda.Remove(anuncio);
                context.SaveChanges();
                return true;
            }
            else return false;
        }

        public List<Anuncio_VendaDTO> GetData()
        {
            

            List<Anuncio_Venda> anuncio = context.Anuncio_Venda.ToList();
            List<Anuncio_VendaDTO> dto = new List<Anuncio_VendaDTO>();
            foreach(Anuncio_Venda  t in anuncio)
            {
                Imovel_Venda_Permuta_Aluguer i = context.Imovel_Venda_Permuta_Aluguer.Find(t.ImovelID);
                Anuncio_VendaDTO tmp = new Anuncio_VendaDTO(t,i);
                dto.Add(tmp);
                
            }
            return dto;
        }

        public Anuncio_VendaDTO GetData(int id)
        {
            Anuncio_Venda a = context.Anuncio_Venda.Find(id);
            Imovel_Venda_Permuta_Aluguer i = context.Imovel_Venda_Permuta_Aluguer.Find(a.ImovelID);
            return new Anuncio_VendaDTO(a,i);
        }

        public bool Update(int id, Anuncio_VendaDTO a)
        {
            var anuncio = context.Anuncio_Venda.Find(id);
            if (a != null)
            {
                anuncio.preco = a.preco;
                anuncio.ImovelID = a.ImovelID;
                anuncio.Imovel = context.Imovel_Venda_Permuta_Aluguer.Find(a.ImovelID);
                anuncio.valido = a.valido;
                anuncio.MediadorID = a.MediadorID;
                anuncio.AnuncioCompraID = a.AnuncioCompraID;
                anuncio.valido = a.valido;
                if (a.tipoImovelID != 0)
                {
                    var imovel = context.Imovel_Venda_Permuta_Aluguer.Find(anuncio.ImovelID);

                    Localizacao l = context.Localizacaos.Find(imovel.localizacaoID);
                    l.local = a.local;
                    l.longitude = a.longitude;
                    l.latitude = a.latitude;
                }

                context.SaveChanges();
                return true;
            }
            else return false;
        }
    }
}
