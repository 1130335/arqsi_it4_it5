﻿using ClassLibrary.DTO;
using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DAL
{
    public class Anuncio_CompraRepository : IRepository<Anuncio_CompraDTO>
    {
        public ImoveisDbContext context;

        public Anuncio_CompraRepository()
        {
            context = new ImoveisDbContext();          
        }

        public Anuncio_CompraDTO Create(Anuncio_CompraDTO anuncio)
        {

            Anuncio_Compra a = new Anuncio_Compra();

            Imovel_Compra tmp = new Imovel_Compra();
            tmp.TipoImovelID = anuncio.tipoImovelID;
            tmp.tipo_de_imovel = context.TipoImovels.Find(anuncio.tipoImovelID);

            Localizacao l = new Localizacao() { latitude = anuncio.latitude, local = anuncio.local, longitude = anuncio.longitude };
            tmp.localizacao = l;

            tmp.areaMin = anuncio.areaMin;
            tmp.areaMAx = anuncio.areaMax;

            a.precoMin = anuncio.precoMin;
            a.precoMax = anuncio.precoMax;

            a.Imovel = tmp;

            a.ApplicationUserID = anuncio.applicationUserID;
            a.valido = "false";

            a.AnuncioVendaID = 0;

            context.Anuncio_Compra.Add(a);
            context.SaveChanges();
            return anuncio;
        }

        public bool Delete(int id)
        {
            var anuncio = context.Anuncio_Compra.Find(id);
            if (anuncio != null)
            {
                context.Anuncio_Compra.Remove(anuncio);
                context.SaveChanges();
                return true;
            }
            else return false;
        }

        public List<Anuncio_CompraDTO> GetData()
        {
            

            List<Anuncio_Compra> anuncio = context.Anuncio_Compra.ToList();
            List<Anuncio_CompraDTO> dto = new List<Anuncio_CompraDTO>();
            foreach(Anuncio_Compra  t in anuncio)
            {
                Anuncio_CompraDTO tmp = new Anuncio_CompraDTO(t);
                dto.Add(tmp);
                
            }
            return dto;
        }

        public Anuncio_CompraDTO GetData(int id)
        {
            return new Anuncio_CompraDTO(context.Anuncio_Compra.Find(id));
        }

        public bool Update(int id, Anuncio_CompraDTO a)
        {
            var anuncio = context.Anuncio_Compra.Find(id);
            if (a != null)
            {
                var imovel = context.Imovel_Compra.Find(anuncio.ImovelID);

                imovel.areaMin = a.areaMin;
                imovel.areaMAx = a.areaMax;
                Localizacao l = context.Localizacaos.Find(imovel.localizacaoID);
                l.local = a.local;
                l.longitude = a.longitude;
                l.latitude = a.latitude;
                imovel.TipoImovelID = a.tipoImovelID;
                imovel.tipo_de_imovel = context.TipoImovels.Find(a.tipoImovelID);
                anuncio.precoMin = a.precoMin;
                anuncio.precoMax = a.precoMax;
                anuncio.Imovel = imovel;
                anuncio.valido = a.valido;
                anuncio.MediadorID = a.MediadorID;
                anuncio.AnuncioVendaID = a.AnuncioVendaID;
                context.SaveChanges();
                return true;
            }
            else return false;
        }
    }
    }

