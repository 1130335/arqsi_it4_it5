namespace ClassLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigracaoInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Alertas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ApplicationUserID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserID)
                .Index(t => t.ApplicationUserID);
            
            CreateTable(
                "dbo.Parametroes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        nome = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Anuncios",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ApplicationUserID = c.String(maxLength: 128),
                        valido = c.String(),
                        MediadorID = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserID)
                .Index(t => t.ApplicationUserID);
            
            CreateTable(
                "dbo.Imovels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TipoImovelID = c.Int(nullable: false),
                        localizacaoID = c.Int(nullable: false),
                        ApplicationUserID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Localizacaos", t => t.localizacaoID, cascadeDelete: true)
                .ForeignKey("dbo.TipoImovels", t => t.TipoImovelID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserID)
                .Index(t => t.TipoImovelID)
                .Index(t => t.localizacaoID)
                .Index(t => t.ApplicationUserID);
            
            CreateTable(
                "dbo.Fotoes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FileName = c.String(maxLength: 255),
                        Content = c.Binary(),
                        ContentType = c.String(maxLength: 100),
                        FileType = c.Int(nullable: false),
                        Imovel_Venda_Permuta_AluguerID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Imovel_Venda_Permuta_Aluguer", t => t.Imovel_Venda_Permuta_AluguerID)
                .Index(t => t.Imovel_Venda_Permuta_AluguerID);
            
            CreateTable(
                "dbo.Localizacaos",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        local = c.String(nullable: false),
                        latitude = c.String(nullable: false),
                        longitude = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TipoImovels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        nome = c.String(nullable: false),
                        subTipo_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TipoImovels", t => t.subTipo_ID)
                .Index(t => t.subTipo_ID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.Anuncio_Aluguer",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        renda = c.Single(nullable: false),
                        ImovelID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Anuncios", t => t.ID)
                .ForeignKey("dbo.Imovel_Venda_Permuta_Aluguer", t => t.ImovelID)
                .Index(t => t.ID)
                .Index(t => t.ImovelID);
            
            CreateTable(
                "dbo.Anuncio_Compra",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        precoMax = c.Single(nullable: false),
                        precoMin = c.Single(nullable: false),
                        ImovelID = c.Int(nullable: false),
                        AnuncioVendaID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Anuncios", t => t.ID)
                .ForeignKey("dbo.Imovel_Compra", t => t.ImovelID)
                .Index(t => t.ID)
                .Index(t => t.ImovelID);
            
            CreateTable(
                "dbo.Anuncio_Permuta",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        preco = c.Single(nullable: false),
                        ImovelID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Anuncios", t => t.ID)
                .ForeignKey("dbo.Imovel_Venda_Permuta_Aluguer", t => t.ImovelID)
                .Index(t => t.ID)
                .Index(t => t.ImovelID);
            
            CreateTable(
                "dbo.Anuncio_Venda",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        preco = c.Single(nullable: false),
                        ImovelID = c.Int(nullable: false),
                        AnuncioCompraID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Anuncios", t => t.ID)
                .ForeignKey("dbo.Imovel_Venda_Permuta_Aluguer", t => t.ImovelID)
                .Index(t => t.ID)
                .Index(t => t.ImovelID);
            
            CreateTable(
                "dbo.Imovel_Compra",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        areaMAx = c.Single(nullable: false),
                        areaMin = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Imovels", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.Imovel_Venda_Permuta_Aluguer",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        area = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Imovels", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.ParametroConstantesAlfaNumerico",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Alerta_ID = c.Int(),
                        str = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Parametroes", t => t.ID)
                .ForeignKey("dbo.Alertas", t => t.Alerta_ID)
                .Index(t => t.ID)
                .Index(t => t.Alerta_ID);
            
            CreateTable(
                "dbo.ParametroConstantesIntervaloNumerico",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Alerta_ID = c.Int(),
                        min = c.Single(nullable: false),
                        max = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Parametroes", t => t.ID)
                .ForeignKey("dbo.Alertas", t => t.Alerta_ID)
                .Index(t => t.ID)
                .Index(t => t.Alerta_ID);
            
            CreateTable(
                "dbo.ParametroFixo",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Alerta_ID = c.Int(),
                        valor_string = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Parametroes", t => t.ID)
                .ForeignKey("dbo.Alertas", t => t.Alerta_ID)
                .Index(t => t.ID)
                .Index(t => t.Alerta_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ParametroFixo", "Alerta_ID", "dbo.Alertas");
            DropForeignKey("dbo.ParametroFixo", "ID", "dbo.Parametroes");
            DropForeignKey("dbo.ParametroConstantesIntervaloNumerico", "Alerta_ID", "dbo.Alertas");
            DropForeignKey("dbo.ParametroConstantesIntervaloNumerico", "ID", "dbo.Parametroes");
            DropForeignKey("dbo.ParametroConstantesAlfaNumerico", "Alerta_ID", "dbo.Alertas");
            DropForeignKey("dbo.ParametroConstantesAlfaNumerico", "ID", "dbo.Parametroes");
            DropForeignKey("dbo.Imovel_Venda_Permuta_Aluguer", "ID", "dbo.Imovels");
            DropForeignKey("dbo.Imovel_Compra", "ID", "dbo.Imovels");
            DropForeignKey("dbo.Anuncio_Venda", "ImovelID", "dbo.Imovel_Venda_Permuta_Aluguer");
            DropForeignKey("dbo.Anuncio_Venda", "ID", "dbo.Anuncios");
            DropForeignKey("dbo.Anuncio_Permuta", "ImovelID", "dbo.Imovel_Venda_Permuta_Aluguer");
            DropForeignKey("dbo.Anuncio_Permuta", "ID", "dbo.Anuncios");
            DropForeignKey("dbo.Anuncio_Compra", "ImovelID", "dbo.Imovel_Compra");
            DropForeignKey("dbo.Anuncio_Compra", "ID", "dbo.Anuncios");
            DropForeignKey("dbo.Anuncio_Aluguer", "ImovelID", "dbo.Imovel_Venda_Permuta_Aluguer");
            DropForeignKey("dbo.Anuncio_Aluguer", "ID", "dbo.Anuncios");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Imovels", "ApplicationUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Imovels", "TipoImovelID", "dbo.TipoImovels");
            DropForeignKey("dbo.Imovels", "localizacaoID", "dbo.Localizacaos");
            DropForeignKey("dbo.Anuncios", "ApplicationUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.TipoImovels", "subTipo_ID", "dbo.TipoImovels");
            DropForeignKey("dbo.Fotoes", "Imovel_Venda_Permuta_AluguerID", "dbo.Imovel_Venda_Permuta_Aluguer");
            DropForeignKey("dbo.Alertas", "ApplicationUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.ParametroFixo", new[] { "Alerta_ID" });
            DropIndex("dbo.ParametroFixo", new[] { "ID" });
            DropIndex("dbo.ParametroConstantesIntervaloNumerico", new[] { "Alerta_ID" });
            DropIndex("dbo.ParametroConstantesIntervaloNumerico", new[] { "ID" });
            DropIndex("dbo.ParametroConstantesAlfaNumerico", new[] { "Alerta_ID" });
            DropIndex("dbo.ParametroConstantesAlfaNumerico", new[] { "ID" });
            DropIndex("dbo.Imovel_Venda_Permuta_Aluguer", new[] { "ID" });
            DropIndex("dbo.Imovel_Compra", new[] { "ID" });
            DropIndex("dbo.Anuncio_Venda", new[] { "ImovelID" });
            DropIndex("dbo.Anuncio_Venda", new[] { "ID" });
            DropIndex("dbo.Anuncio_Permuta", new[] { "ImovelID" });
            DropIndex("dbo.Anuncio_Permuta", new[] { "ID" });
            DropIndex("dbo.Anuncio_Compra", new[] { "ImovelID" });
            DropIndex("dbo.Anuncio_Compra", new[] { "ID" });
            DropIndex("dbo.Anuncio_Aluguer", new[] { "ImovelID" });
            DropIndex("dbo.Anuncio_Aluguer", new[] { "ID" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.TipoImovels", new[] { "subTipo_ID" });
            DropIndex("dbo.Fotoes", new[] { "Imovel_Venda_Permuta_AluguerID" });
            DropIndex("dbo.Imovels", new[] { "ApplicationUserID" });
            DropIndex("dbo.Imovels", new[] { "localizacaoID" });
            DropIndex("dbo.Imovels", new[] { "TipoImovelID" });
            DropIndex("dbo.Anuncios", new[] { "ApplicationUserID" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Alertas", new[] { "ApplicationUserID" });
            DropTable("dbo.ParametroFixo");
            DropTable("dbo.ParametroConstantesIntervaloNumerico");
            DropTable("dbo.ParametroConstantesAlfaNumerico");
            DropTable("dbo.Imovel_Venda_Permuta_Aluguer");
            DropTable("dbo.Imovel_Compra");
            DropTable("dbo.Anuncio_Venda");
            DropTable("dbo.Anuncio_Permuta");
            DropTable("dbo.Anuncio_Compra");
            DropTable("dbo.Anuncio_Aluguer");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.TipoImovels");
            DropTable("dbo.Localizacaos");
            DropTable("dbo.Fotoes");
            DropTable("dbo.Imovels");
            DropTable("dbo.Anuncios");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Parametroes");
            DropTable("dbo.Alertas");
        }
    }
}
