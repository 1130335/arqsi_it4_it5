﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.DAL;
using ClassLibrary.Models;
using ClassLibrary.DTO;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class FotoesController : ApiController
    {
        private IRepository<FotoDTO> repo = new FotoRepository();

        // GET: api/Fotoes
        public IEnumerable<FotoDTO> GetFotos()
        {
            return repo.GetData().ToList();
        }

        // GET: api/Fotoes/5
        [ResponseType(typeof(FotoDTO))]
        public IHttpActionResult GetFoto(int id)
        {
            return Ok(repo.GetData(id));
        }

        // PUT: api/Fotoes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFoto(int id, FotoDTO foto)
        {
            repo.Update(id,foto);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Fotoes
        [ResponseType(typeof(FotoDTO))]
        public IHttpActionResult PostFoto(FotoDTO foto)
        {
            repo.Create(foto);

            return CreatedAtRoute("DefaultApi", new { id = foto.ID }, foto);
        }

        // DELETE: api/Fotoes/5
        [ResponseType(typeof(Foto))]
        public IHttpActionResult DeleteFoto(int id)
        {
            repo.Delete(id);

            return Ok();
        }

        
    }
}