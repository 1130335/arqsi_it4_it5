﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.DAL;
using ClassLibrary.Models;
using ClassLibrary.DTO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class AlertasController : ApiController
    {
        private IRepository<AlertaDTO> repo = new AlertaRepository();

        // GET: api/Alertas
        public IEnumerable<AlertaDTO> GetAlertas()
        {
            List<AlertaDTO> ret = new List<AlertaDTO>();
            foreach (AlertaDTO alerta in repo.GetData())
            {
                if (alerta.applicationUserID == User.Identity.GetUserId())
                {
                    ret.Add(alerta);
                }
            }
            return ret;
        }

        // GET: api/Alertas/5
        [ResponseType(typeof(Alerta))]
        public IHttpActionResult GetAlerta(int id)
        {
            AlertaDTO alerta = repo.GetData(id);
            if (alerta.applicationUserID != User.Identity.GetUserId())
            {
                return Ok();
            }
            return Ok(repo.GetData(id));
        }

        // PUT: api/Alertas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAlerta(int id, AlertaDTO alerta)
        {
            AlertaDTO a = repo.GetData(id);
            if (User.Identity.GetUserId() != a.applicationUserID)
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }
            repo.Update(id, alerta);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Alertas
        [ResponseType(typeof(AlertaDTO))]
        public IHttpActionResult PostAlerta(AlertaDTO alerta)
        {
            alerta.applicationUserID = User.Identity.GetUserId();
            repo.Create(alerta);
            return CreatedAtRoute("DefaultApi", new { id = alerta.ID }, alerta);
        }

        // DELETE: api/Alertas/5
        [ResponseType(typeof(AlertaDTO))]
        public IHttpActionResult DeleteAlerta(int id)
        {
            AlertaDTO alerta = repo.GetData(id);
            if (User.Identity.GetUserId() != alerta.applicationUserID)
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }
            repo.Delete(id);
            return Ok();
        }

       
    }
}