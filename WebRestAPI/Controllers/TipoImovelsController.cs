﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.DAL;
using ClassLibrary.Models;
using ClassLibrary.DTO;

namespace WebRestAPI.Controllers
{
   [Authorize]
    public class TipoImovelsController : ApiController
    {
        private IRepository<TipoImovelDTO> repo = new TipoImovelRepository();

        // GET: api/TipoImovels
        public IEnumerable<TipoImovelDTO> GetTipoImovels()
        {
            return repo.GetData();
        }

        // GET: api/TipoImovels/5
        [ResponseType(typeof(TipoImovelDTO))]
        public IHttpActionResult GetTipoImovel(int id)
        {
            return Ok(repo.GetData(id));
        }

        // PUT: api/TipoImovels/5
       [Authorize(Roles = "Admin")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTipoImovel(int id, TipoImovelDTO tipoImovel)
        {
            repo.Update(id, tipoImovel);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TipoImovels
       [Authorize(Roles="Admin")]
        [ResponseType(typeof(TipoImovelDTO))]
        public IHttpActionResult PostTipoImovel(TipoImovelDTO tipoImovel)
        {

            repo.Create(tipoImovel);
            return CreatedAtRoute("DefaultApi", new { id = tipoImovel.ID }, tipoImovel);
        }

        // DELETE: api/TipoImovels/5
       [Authorize(Roles = "Admin")]
        [ResponseType(typeof(TipoImovel))]
        public IHttpActionResult DeleteTipoImovel(int id)
        {
            if(repo.Delete(id)==false)
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }
            return Ok();
        }

    }
}