﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.DAL;
using ClassLibrary.Models;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class LocalizacaosController : ApiController
    {
        private ImoveisDbContext db = new ImoveisDbContext();

        // GET: api/Localizacaos
        public IQueryable<Localizacao> GetLocalizacaos()
        {
            return db.Localizacaos;
        }

        // GET: api/Localizacaos/5
        [ResponseType(typeof(Localizacao))]
        public IHttpActionResult GetLocalizacao(int id)
        {
            Localizacao localizacao = db.Localizacaos.Find(id);
            if (localizacao == null)
            {
                return NotFound();
            }

            return Ok(localizacao);
        }

        // PUT: api/Localizacaos/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLocalizacao(int id, Localizacao localizacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != localizacao.ID)
            {
                return BadRequest();
            }

            db.Entry(localizacao).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LocalizacaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Localizacaos
        [ResponseType(typeof(Localizacao))]
        public IHttpActionResult PostLocalizacao(Localizacao localizacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Localizacaos.Add(localizacao);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = localizacao.ID }, localizacao);
        }

        // DELETE: api/Localizacaos/5
        [ResponseType(typeof(Localizacao))]
        public IHttpActionResult DeleteLocalizacao(int id)
        {
            Localizacao localizacao = db.Localizacaos.Find(id);
            if (localizacao == null)
            {
                return NotFound();
            }

            db.Localizacaos.Remove(localizacao);
            db.SaveChanges();

            return Ok(localizacao);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LocalizacaoExists(int id)
        {
            return db.Localizacaos.Count(e => e.ID == id) > 0;
        }
    }
}