﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.DAL;
using ClassLibrary.Models;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class ParametroesController : ApiController
    {
        private ImoveisDbContext db = new ImoveisDbContext();

        // GET: api/Parametroes
        public IQueryable<Parametro> GetParametroes()
        {
            return db.Parametroes;
        }

        // GET: api/Parametroes/5
        [ResponseType(typeof(Parametro))]
        public IHttpActionResult GetParametro(int id)
        {
            Parametro parametro = db.Parametroes.Find(id);
            if (parametro == null)
            {
                return NotFound();
            }

            return Ok(parametro);
        }

        // PUT: api/Parametroes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutParametro(int id, Parametro parametro)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != parametro.ID)
            {
                return BadRequest();
            }

            db.Entry(parametro).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParametroExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Parametroes
        [ResponseType(typeof(Parametro))]
        public IHttpActionResult PostParametro(Parametro parametro)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Parametroes.Add(parametro);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = parametro.ID }, parametro);
        }

        // DELETE: api/Parametroes/5
        [ResponseType(typeof(Parametro))]
        public IHttpActionResult DeleteParametro(int id)
        {
            Parametro parametro = db.Parametroes.Find(id);
            if (parametro == null)
            {
                return NotFound();
            }

            db.Parametroes.Remove(parametro);
            db.SaveChanges();

            return Ok(parametro);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ParametroExists(int id)
        {
            return db.Parametroes.Count(e => e.ID == id) > 0;
        }
    }
}