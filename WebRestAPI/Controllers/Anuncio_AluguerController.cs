﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.DAL;
using ClassLibrary.Models;
using ClassLibrary.DTO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class Anuncio_AluguerController : ApiController
    {
        private IRepository<Anuncio_AluguerDTO> repo = new Anuncio_AluguerRepository();

        // GET: api/TipoImovels
        public IEnumerable<Anuncio_AluguerDTO> GetAluguer()
        {
            return repo.GetData();
        }

        // GET: api/TipoImovels/5
        [ResponseType(typeof(Anuncio_AluguerDTO))]
        public IHttpActionResult GetAluguer(int id)
        {
            return Ok(repo.GetData(id));
        }

        // PUT: api/TipoImovels/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAluguer(int id, Anuncio_AluguerDTO anuncio)
        {
            Anuncio_AluguerDTO a = repo.GetData(id);
            if (User.Identity.GetUserId() != a.applicationUserID && !User.IsInRole("Mediador"))
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }
            if (anuncio.valido == "true")
            {
                anuncio.MediadorID = User.Identity.GetUserId();
            }
            repo.Update(id, anuncio);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TipoImovels
        [ResponseType(typeof(Anuncio_AluguerDTO))]
        public IHttpActionResult PostAluguer(Anuncio_AluguerDTO anuncio)
        {
            anuncio.applicationUserID = User.Identity.GetUserId();
            repo.Create(anuncio);
            return CreatedAtRoute("DefaultApi", new { id = anuncio.ID }, anuncio);
        }

        // DELETE: api/TipoImovels/5
        [ResponseType(typeof(Anuncio_AluguerDTO))]
        public IHttpActionResult DeleteAluguer(int id)
        {
            Anuncio_AluguerDTO anuncio_Aluguer = repo.GetData(id);
            if (User.Identity.GetUserId() != anuncio_Aluguer.applicationUserID && !User.IsInRole("Mediador"))
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }
            repo.Delete(id);
            return Ok();
        }
    }
}