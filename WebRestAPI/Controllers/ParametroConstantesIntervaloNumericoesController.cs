﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.DAL;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class ParametroConstantesIntervaloNumericoesController : ApiController
    {
        private ImoveisDbContext db = new ImoveisDbContext();

        // GET: api/ParametroConstantesIntervaloNumericoes
        public IQueryable<ParametroConstantesIntervaloNumerico> GetParametroes()
        {
            return db.ParametroConstantesIntervaloNumericoes;
        }

        // GET: api/ParametroConstantesIntervaloNumericoes/5
        [ResponseType(typeof(ParametroConstantesIntervaloNumerico))]
        public IHttpActionResult GetParametroConstantesIntervaloNumerico(int id)
        {
            ParametroConstantesIntervaloNumerico parametroConstantesIntervaloNumerico = db.ParametroConstantesIntervaloNumericoes.Find(id);
            if (parametroConstantesIntervaloNumerico == null)
            {
                return NotFound();
            }

            return Ok(parametroConstantesIntervaloNumerico);
        }

        // PUT: api/ParametroConstantesIntervaloNumericoes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutParametroConstantesIntervaloNumerico(int id, ParametroConstantesIntervaloNumerico parametroConstantesIntervaloNumerico)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != parametroConstantesIntervaloNumerico.ID)
            {
                return BadRequest();
            }

            db.Entry(parametroConstantesIntervaloNumerico).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParametroConstantesIntervaloNumericoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ParametroConstantesIntervaloNumericoes
        [ResponseType(typeof(ParametroConstantesIntervaloNumerico))]
        public IHttpActionResult PostParametroConstantesIntervaloNumerico(ParametroConstantesIntervaloNumerico parametroConstantesIntervaloNumerico)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ParametroConstantesIntervaloNumericoes.Add(parametroConstantesIntervaloNumerico);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = parametroConstantesIntervaloNumerico.ID }, parametroConstantesIntervaloNumerico);
        }

        // DELETE: api/ParametroConstantesIntervaloNumericoes/5
        [ResponseType(typeof(ParametroConstantesIntervaloNumerico))]
        public IHttpActionResult DeleteParametroConstantesIntervaloNumerico(int id)
        {
            ParametroConstantesIntervaloNumerico parametroConstantesIntervaloNumerico = db.ParametroConstantesIntervaloNumericoes.Find(id);
            if (parametroConstantesIntervaloNumerico == null)
            {
                return NotFound();
            }

            db.ParametroConstantesIntervaloNumericoes.Remove(parametroConstantesIntervaloNumerico);
            db.SaveChanges();

            return Ok(parametroConstantesIntervaloNumerico);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ParametroConstantesIntervaloNumericoExists(int id)
        {
            return db.ParametroConstantesIntervaloNumericoes.Count(e => e.ID == id) > 0;
        }
    }
}