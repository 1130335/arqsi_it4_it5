﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.DAL;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class ParametroFixoesController : ApiController
    {
        private ImoveisDbContext db = new ImoveisDbContext();

        // GET: api/ParametroFixoes
        public IQueryable<ParametroFixo> GetParametroes()
        {
            return db.ParametroFixoes;
        }

        // GET: api/ParametroFixoes/5
        [ResponseType(typeof(ParametroFixo))]
        public IHttpActionResult GetParametroFixo(int id)
        {
            ParametroFixo parametroFixo = db.ParametroFixoes.Find(id);
            if (parametroFixo == null)
            {
                return NotFound();
            }

            return Ok(parametroFixo);
        }

        // PUT: api/ParametroFixoes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutParametroFixo(int id, ParametroFixo parametroFixo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != parametroFixo.ID)
            {
                return BadRequest();
            }

            db.Entry(parametroFixo).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParametroFixoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ParametroFixoes
        [ResponseType(typeof(ParametroFixo))]
        public IHttpActionResult PostParametroFixo(ParametroFixo parametroFixo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ParametroFixoes.Add(parametroFixo);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = parametroFixo.ID }, parametroFixo);
        }

        // DELETE: api/ParametroFixoes/5
        [ResponseType(typeof(ParametroFixo))]
        public IHttpActionResult DeleteParametroFixo(int id)
        {
            ParametroFixo parametroFixo = db.ParametroFixoes.Find(id);
            if (parametroFixo == null)
            {
                return NotFound();
            }

            db.ParametroFixoes.Remove(parametroFixo);
            db.SaveChanges();

            return Ok(parametroFixo);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ParametroFixoExists(int id)
        {
            return db.ParametroFixoes.Count(e => e.ID == id) > 0;
        }
    }
}