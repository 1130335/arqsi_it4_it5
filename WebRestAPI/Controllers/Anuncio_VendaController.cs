﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.DAL;
using ClassLibrary.Models;
using ClassLibrary.DTO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class Anuncio_VendaController : ApiController
    {
        private IRepository<Anuncio_VendaDTO> repo = new Anuncio_VendaRepository();

        // GET: api/TipoImovels
        public IEnumerable<Anuncio_VendaDTO> GetVenda()
        {
            return repo.GetData();
        }

        // GET: api/TipoImovels/5
        [ResponseType(typeof(Anuncio_VendaDTO))]
        public IHttpActionResult GetVenda(int id)
        {
            return Ok(repo.GetData(id));
        }

        // PUT: api/TipoImovels/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVenda(int id, Anuncio_VendaDTO anuncio)
        {
            Anuncio_VendaDTO a = repo.GetData(id);
            if (User.Identity.GetUserId() != a.applicationUserID && !User.IsInRole("Mediador"))
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }
            if(anuncio.valido == "true")
            {
                anuncio.MediadorID = User.Identity.GetUserId();
            }
            repo.Update(id, anuncio);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TipoImovels
        [ResponseType(typeof(Anuncio_VendaDTO))]
        public IHttpActionResult PostVenda(Anuncio_VendaDTO anuncio)
        {

            anuncio.applicationUserID = User.Identity.GetUserId();
            repo.Create(anuncio);
            return CreatedAtRoute("DefaultApi", new { id = anuncio.ID }, anuncio);
        }

        // DELETE: api/TipoImovels/5
        [ResponseType(typeof(Anuncio_VendaDTO))]
        public IHttpActionResult DeleteVenda(int id)
        {
            Anuncio_VendaDTO a = repo.GetData(id);
            if (User.Identity.GetUserId() != a.applicationUserID && !User.IsInRole("Mediador"))
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }
            repo.Delete(id);
            return Ok();
        }
    }
}