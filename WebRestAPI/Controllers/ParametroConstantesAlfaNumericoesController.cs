﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.DAL;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class ParametroConstantesAlfaNumericoesController : ApiController
    {
        private ImoveisDbContext db = new ImoveisDbContext();

        // GET: api/ParametroConstantesAlfaNumericoes
        public IQueryable<ParametroConstantesAlfaNumerico> GetParametroes()
        {
            return db.ParametroConstantesAlfaNumericoes;
        }

        // GET: api/ParametroConstantesAlfaNumericoes/5
        [ResponseType(typeof(ParametroConstantesAlfaNumerico))]
        public IHttpActionResult GetParametroConstantesAlfaNumerico(int id)
        {
            ParametroConstantesAlfaNumerico parametroConstantesAlfaNumerico = db.ParametroConstantesAlfaNumericoes.Find(id);
            if (parametroConstantesAlfaNumerico == null)
            {
                return NotFound();
            }

            return Ok(parametroConstantesAlfaNumerico);
        }

        // PUT: api/ParametroConstantesAlfaNumericoes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutParametroConstantesAlfaNumerico(int id, ParametroConstantesAlfaNumerico parametroConstantesAlfaNumerico)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != parametroConstantesAlfaNumerico.ID)
            {
                return BadRequest();
            }

            db.Entry(parametroConstantesAlfaNumerico).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParametroConstantesAlfaNumericoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ParametroConstantesAlfaNumericoes
        [ResponseType(typeof(ParametroConstantesAlfaNumerico))]
        public IHttpActionResult PostParametroConstantesAlfaNumerico(ParametroConstantesAlfaNumerico parametroConstantesAlfaNumerico)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ParametroConstantesAlfaNumericoes.Add(parametroConstantesAlfaNumerico);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = parametroConstantesAlfaNumerico.ID }, parametroConstantesAlfaNumerico);
        }

        // DELETE: api/ParametroConstantesAlfaNumericoes/5
        [ResponseType(typeof(ParametroConstantesAlfaNumerico))]
        public IHttpActionResult DeleteParametroConstantesAlfaNumerico(int id)
        {
            ParametroConstantesAlfaNumerico parametroConstantesAlfaNumerico = db.ParametroConstantesAlfaNumericoes.Find(id);
            if (parametroConstantesAlfaNumerico == null)
            {
                return NotFound();
            }

            db.ParametroConstantesAlfaNumericoes.Remove(parametroConstantesAlfaNumerico);
            db.SaveChanges();

            return Ok(parametroConstantesAlfaNumerico);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ParametroConstantesAlfaNumericoExists(int id)
        {
            return db.ParametroConstantesAlfaNumericoes.Count(e => e.ID == id) > 0;
        }
    }
}