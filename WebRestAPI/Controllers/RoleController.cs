﻿using ClassLibrary.DAL;
using ClassLibrary.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class RoleController : ApiController
    {
        // GET api/<controller>
        public List<string> Get()
        {
            var context = new ImoveisDbContext();
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            string id = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(id);
            List<IdentityUserRole> l =  user.Roles.ToList();
            List<string> list = new List<string>();
            for (int i = 0; i < l.Count;i++)
            {
                list.Add(roleManager.FindById(l[i].RoleId).Name);
            }
                return list;
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}
