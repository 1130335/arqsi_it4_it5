﻿using ClassLibrary.DAL;
using ClassLibrary.DTO;
using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace WebRestAPI.Controllers
{
    public class VisitasController : ApiController
    {
        private IRepository<VisitaDTO> repo = new VisitaRepository();

        // GET: api/Visitas
        public IEnumerable<VisitaDTO> GetVisitas()
        {
            return repo.GetData().ToList();
        }

        // GET: api/Visitas/5
        [ResponseType(typeof(VisitaDTO))]
        public IHttpActionResult GetVisita(int id)
        {
            return Ok(repo.GetData(id));
        }

        // PUT: api/Visitas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVisita(int id, VisitaDTO visita)
        {
            repo.Update(id, visita);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Visitas
        [ResponseType(typeof(VisitaDTO))]
        public IHttpActionResult PostVisita(VisitaDTO visita)
        {
            repo.Create(visita);

            return CreatedAtRoute("DefaultApi", new { id = visita.ID }, visita);
        }

        // DELETE: api/Visitas/5
        [ResponseType(typeof(Visita))]
        public IHttpActionResult DeleteVisita(int id)
        {
            repo.Delete(id);

            return Ok();
        }


    }
}