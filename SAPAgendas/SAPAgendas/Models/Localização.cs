﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SAPAgendas.Models
{
    [DataContract]
    public class Localização
    {
        [DataMember]
        public int LocalizaçãoID { get; set; }
        [DataMember]
        public String Nome { get; set; }
        //public List<Imoveis> Imoveis { get; set; }
    }
}