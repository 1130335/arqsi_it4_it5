﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SAPAgendas.Models
{
    [DataContract]
    public class Distancia
    {
        [Key]
        [DataMember]
        public int DistanciaID { get; set; }
        [DataMember]
        public String LocA { get; set; }
        [DataMember]
        public String LocB { get; set; }
        [DataMember]
        public float DistanciaEmTempo { get; set; }
    }
}