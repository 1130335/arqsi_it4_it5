﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SAPAgendas.Models;

namespace SAPAgendas.Controllers
{
    public class LocalizaçãoController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Localização
        [ResponseType(typeof(List<Localização>))]
        public List<Localização> GetLocalização()
        {
            return db.Localização.ToList();
        }

        // GET: api/Localização/5
        [ResponseType(typeof(Localização))]
        public async Task<IHttpActionResult> GetLocalização(int id)
        {
            Localização localização = await db.Localização.FindAsync(id);
            if (localização == null)
            {
                return NotFound();
            }

            return Ok(localização);
        }

        // PUT: api/Localização/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutLocalização(int id, Localização localização)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != localização.LocalizaçãoID)
            {
                return BadRequest();
            }

            db.Entry(localização).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LocalizaçãoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Localização
        [ResponseType(typeof(Localização))]
        public async Task<IHttpActionResult> PostLocalização(Localização localização)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Localização.Add(localização);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = localização.LocalizaçãoID }, localização);
        }

        // DELETE: api/Localização/5
        [ResponseType(typeof(Localização))]
        public async Task<IHttpActionResult> DeleteLocalização(int id)
        {
            Localização localização = await db.Localização.FindAsync(id);
            if (localização == null)
            {
                return NotFound();
            }

            db.Localização.Remove(localização);
            await db.SaveChangesAsync();

            return Ok(localização);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LocalizaçãoExists(int id)
        {
            return db.Localização.Count(e => e.LocalizaçãoID == id) > 0;
        }
    }
}