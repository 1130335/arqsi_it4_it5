﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SAPAgendas.Models;

namespace SAPAgendas.Controllers
{
    public class DistanciasController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Distancias
        public IEnumerable<Distancia> GetDistancia()
        {
            return db.Distancia.ToList();
        }

        // GET: api/Distancias/5
        [ResponseType(typeof(Distancia))]
        public async Task<IHttpActionResult> GetDistancia(int id)
        {
            Distancia distancia = await db.Distancia.FindAsync(id);
            if (distancia == null)
            {
                return NotFound();
            }

            return Ok(distancia);
        }

        // PUT: api/Distancias/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDistancia(int id, Distancia distancia)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != distancia.DistanciaID)
            {
                return BadRequest();
            }

            db.Entry(distancia).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DistanciaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Distancias
        [ResponseType(typeof(Distancia))]
        public async Task<IHttpActionResult> PostDistancia(Distancia distancia)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Distancia.Add(distancia);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = distancia.DistanciaID }, distancia);
        }

        // DELETE: api/Distancias/5
        [ResponseType(typeof(Distancia))]
        public async Task<IHttpActionResult> DeleteDistancia(int id)
        {
            Distancia distancia = await db.Distancia.FindAsync(id);
            if (distancia == null)
            {
                return NotFound();
            }

            db.Distancia.Remove(distancia);
            await db.SaveChangesAsync();

            return Ok(distancia);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DistanciaExists(int id)
        {
            return db.Distancia.Count(e => e.DistanciaID == id) > 0;
        }
    }
}