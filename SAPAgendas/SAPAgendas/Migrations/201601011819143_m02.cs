namespace SAPAgendas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m02 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Distancias", "LocA_LocalizaçãoID", "dbo.Localização");
            DropForeignKey("dbo.Distancias", "LocB_LocalizaçãoID", "dbo.Localização");
            DropIndex("dbo.Distancias", new[] { "LocA_LocalizaçãoID" });
            DropIndex("dbo.Distancias", new[] { "LocB_LocalizaçãoID" });
            AddColumn("dbo.Distancias", "LocA", c => c.String());
            AddColumn("dbo.Distancias", "LocB", c => c.String());
            DropColumn("dbo.Distancias", "LocA_LocalizaçãoID");
            DropColumn("dbo.Distancias", "LocB_LocalizaçãoID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Distancias", "LocB_LocalizaçãoID", c => c.Int());
            AddColumn("dbo.Distancias", "LocA_LocalizaçãoID", c => c.Int());
            DropColumn("dbo.Distancias", "LocB");
            DropColumn("dbo.Distancias", "LocA");
            CreateIndex("dbo.Distancias", "LocB_LocalizaçãoID");
            CreateIndex("dbo.Distancias", "LocA_LocalizaçãoID");
            AddForeignKey("dbo.Distancias", "LocB_LocalizaçãoID", "dbo.Localização", "LocalizaçãoID");
            AddForeignKey("dbo.Distancias", "LocA_LocalizaçãoID", "dbo.Localização", "LocalizaçãoID");
        }
    }
}
