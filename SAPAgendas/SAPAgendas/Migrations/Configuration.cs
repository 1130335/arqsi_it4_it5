namespace SAPAgendas.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SAPAgendas.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SAPAgendas.Models.ApplicationDbContext context)
        {
            context.Localização.AddOrUpdate(
                new Localização()
                {
                    Nome = "Valongo"
                },

                new Localização()
                {
                    Nome = "Rio Tinto"
                },

                new Localização()
                {
                    Nome = "Ermesinde"
                }
                );
        }
    }
}
