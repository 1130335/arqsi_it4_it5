﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace APPClienteIt3.Models
{
    public class Alerta
    {

        public int ID { get; set; }
        public string applicationUserID { get; set; }

        public List<List<String>> fixo { get; set; }
        public List<List<String>> parametroAlfaNumerico { get; set; }
        public List<List<String>> parametroIntervaloNumerico { get; set; }
    }
}