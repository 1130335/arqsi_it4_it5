﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace APPClienteIt3.Models
{
    public class Imovel_Venda_Permuta_Aluguer
    {
        public int ID { get; set; }
        public string applicationUserID { get; set; }
        public int tipoImovelID { get; set; }

        [DisplayName("Tipo de Imovel")]
        public string tipoImovel { get; set; }
        public int localizacaoID { get; set; }

        [DisplayName("Local")]
        public string local { get; set; }

        [DisplayName("Latitude")]
        public string latitude { get; set; }

        [DisplayName("Longitude")]
        public string longitude { get; set; }

        [DisplayName("Área")]
        public float area { get; set; }

        public int fotoID { get; set; }
        public string fotoContent { get; set; }
        public string fotoContentType { get; set; }
    }
}