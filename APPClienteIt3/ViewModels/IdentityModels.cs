﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace APPClienteIt3.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            
        }

        public static ApplicationDbContext Create()
        {

            

            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<APPClienteIt3.Models.TipoImovel> TipoImovels { get; set; }
        
        public System.Data.Entity.DbSet<APPClienteIt3.Models.Imovel_Compra> Imovel_Compra { get; set; }

        public System.Data.Entity.DbSet<APPClienteIt3.Models.Anuncio_Compra> Anuncio_Compra { get; set; }

        public System.Data.Entity.DbSet<APPClienteIt3.Models.Imovel_Venda_Permuta_Aluguer> Imovel_Venda_Permuta_Aluguer { get; set; }

        public System.Data.Entity.DbSet<APPClienteIt3.Models.Anuncio_Venda> Anuncio_Venda { get; set; }

        public System.Data.Entity.DbSet<APPClienteIt3.Models.Anuncio_Permuta> Anuncio_Permuta { get; set; }

        public System.Data.Entity.DbSet<APPClienteIt3.Models.Anuncio_Aluguer> Anuncio_Aluguer { get; set; }

        public System.Data.Entity.DbSet<ParametroFixo> ParametroFixoes { get; set; }

        public System.Data.Entity.DbSet<APPClienteIt3.Models.Foto> Fotoes { get; set; }

        public System.Data.Entity.DbSet<APPClienteIt3.Models.Alerta> Alertas { get; set; }

    }
}