﻿using APPClienteIt3.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace APPClienteIt3.Models
{
    public abstract class Parametro
    {

        [HiddenInput(DisplayValue = false)]
        public int ID { get; set; }

        [DisplayName("Nome")]
        public string nome {get; set; }
        
    }
}
[Table("ParametroFixo")]
public class ParametroFixo : Parametro
{
    public string valor_string { get; set; }
}


[Table("ParametroConstantesAlfaNumerico")]
public class ParametroConstantesAlfaNumerico : Parametro
{
    public string str { get; set; }
}

[Table("ParametroConstantesIntervaloNumerico")]
public class ParametroConstantesIntervaloNumerico : Parametro
{
    
    [DisplayName("Mínimo")]
    public float min { get; set; }

    [DisplayName("Máximo")]
    public float max { get; set; }
}
