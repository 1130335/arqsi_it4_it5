﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace APPClienteIt3.ViewModels
{
    public class Visita
    {
        public int ID { get; set; }

        public int imovelID { get; set; }
        public string tipoimovel { get; set; }

        [DisplayName("Hora Disponivel Início Visita:")]
        public float HoraInicio { get; set; }
        [DisplayName("Hora Disponivel Fim Visita: ")]
        public float HoraFim { get; set; }

        [DisplayName("Dia:")]
        public int Dia { get; set; }
        [DisplayName("Mês:")]
        public int Mes { get; set; }
        [DisplayName("Ano:")]
        public int Ano { get; set; }
    }
}