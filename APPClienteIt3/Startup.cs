﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(APPClienteIt3.Startup))]
namespace APPClienteIt3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
