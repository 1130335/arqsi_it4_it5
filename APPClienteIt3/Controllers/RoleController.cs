﻿using APPClienteIt3.Helpers;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace APPClienteIt3.Controllers
{
    public class RoleController : Controller
    {
        // GET: Role
        public async Task<List<string>> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Role/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var roles =
                JsonConvert.DeserializeObject<IEnumerable<string>>(content);
                return roles.ToList();
            }
            else
            {
                return null;
            }
        }
    }
}