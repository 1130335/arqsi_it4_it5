﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APPClienteIt3.Models;
using APPClienteIt3.Helpers;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace APPClienteIt3.Controllers
{
    public class Anuncio_CompraController : Controller
    {
        //private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Anuncio_Compra
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Compra");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anunciosCompra =
                JsonConvert.DeserializeObject<IEnumerable<Anuncio_Compra>>(content);
                return View(anunciosCompra);
            }
            else
            {
                return RedirectToAction("Noauto", "Home");
            }
        }

        public async Task<IEnumerable<Anuncio_Compra>> getAll()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Compra/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anunciosCompra =
                JsonConvert.DeserializeObject<IEnumerable<Anuncio_Compra>>(content);
                return anunciosCompra;
            }
            else
            {
                return null;
            }
        }

        // GET: Anuncio_Compra/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Compra/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncioCompra = JsonConvert.DeserializeObject<Anuncio_Compra>(content);
                if (anuncioCompra == null) return HttpNotFound();
                return View(anuncioCompra);
            }
            else
            {
                return RedirectToAction("Noauto", "Home");
            }
        }

        // GET: Anuncio_Compra/Create
        public async Task<ActionResult> Create()
        {
            await PopulateDropDownListTipoImovel(0);
            return View();
        }

        // POST: Anuncio_Compra/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,precoMin,precoMax,areaMin,areaMax,local,latitude,longitude,TipoImovelID")] Anuncio_Compra anuncioCompra, int TipoImovelID)
        {
            try
            {

                var client = WebApiHttpClient.GetClient();
                string anuncioCompraJSON = JsonConvert.SerializeObject(anuncioCompra);
                HttpContent content = new StringContent(anuncioCompraJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Anuncio_Compra/", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index", "Anuncios");
                }
                else
                {
                    return RedirectToAction("Erro", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Anuncio_Compra/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Compra/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncioCompra = JsonConvert.DeserializeObject<Anuncio_Compra>(content);
                if (anuncioCompra == null) return HttpNotFound();

                if (anuncioCompra.tipoImovel != null)
                {
                    await PopulateDropDownListTipoImovel(anuncioCompra.tipoImovelID);
                }
                else
                {
                    await PopulateDropDownListTipoImovel();
                }

                return View(anuncioCompra);
            }
            return RedirectToAction("Noauto", "Home");
        }

        // POST: Anuncio_Compra/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,precoMin,precoMax,areaMin,areaMax,local,latitude,longitude,TipoImovelID")] Anuncio_Compra anuncioCompra, string TipoImovelID)
        {
            try
            {


                var client = WebApiHttpClient.GetClient();
                string anuncioCompraJSON = JsonConvert.SerializeObject(anuncioCompra);
                HttpContent content = new StringContent(anuncioCompraJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response =
                await client.PutAsync("api/Anuncio_Compra/" + anuncioCompra.ID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index", "Anuncios");
                }
                else
                {
                    return RedirectToAction("Erro", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Anuncio_Compra/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Compra/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncioCompra = JsonConvert.DeserializeObject<Anuncio_Compra>(content);
                if (anuncioCompra == null) return HttpNotFound();
                return View(anuncioCompra);
            }
            return RedirectToAction("Noauto", "Home");
        }

        // POST: Anuncio_Compra/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Anuncio_Compra/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index", "Anuncios");
                }
                else
                {
                    return RedirectToAction("Noauto", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }
        private async Task<Object> PopulateDropDownListTipoImovel(int? ID = null)
        {
            TipoImovelsController ctrl = new TipoImovelsController();
            IEnumerable<TipoImovel> list = await ctrl.getAll();
            TipoImovel TipoImovelID;
            if (ID.Value == 0)
            {
                TipoImovelID = null;
            }
            else
            {
                TipoImovelID = await ctrl.getTipoImovelbyID(ID.Value);
            }

            if (list == null)
            {
                list = new List<TipoImovel>();
            }
            if (TipoImovelID == null)
            {
                return ViewBag.TipoImovelID = new SelectList(list, "ID", "nome", TipoImovelID);
            }
            else
            {
                return ViewBag.TipoImovelID = new SelectList(list, "ID", "nome", TipoImovelID.ID);
            }
        }
    }
}
