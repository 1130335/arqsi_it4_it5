﻿using APPClienteIt3.Helpers;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace APPClienteIt3.Controllers
{
    public class ApplicationUserController : Controller
    {
        // GET: Role
        public async Task<string> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/ApplicationUser/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var id =
                JsonConvert.DeserializeObject<string>(content);
                return id;
            }
            else
            {
                return null;
            }
        }
    }
}
