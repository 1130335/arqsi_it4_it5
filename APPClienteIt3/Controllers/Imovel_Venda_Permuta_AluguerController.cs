﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APPClienteIt3.Models;
using APPClienteIt3.Helpers;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;


namespace APPClienteIt3.Controllers
{
    public class Imovel_Venda_Permuta_AluguerController : Controller
    {

        // GET: Imovel_Venda_Permuta_Aluguer
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel_Venda_Permuta_Aluguer/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovelVendaPermutaAluguer =
                JsonConvert.DeserializeObject<IEnumerable<Imovel_Venda_Permuta_Aluguer>>(content);
                return View(imovelVendaPermutaAluguer);
            }
            else
            {
                return RedirectToAction("Noauto", "Home");
            }
        }


        public async Task<IEnumerable<Imovel_Venda_Permuta_Aluguer>> getAll()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel_Venda_Permuta_Aluguer/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovelVendaPermutaAluguer =
                JsonConvert.DeserializeObject<IEnumerable<Imovel_Venda_Permuta_Aluguer>>(content);
                return imovelVendaPermutaAluguer;
            }
            else
            {
                return null;
            }
        }

        // GET: Imovel_Venda_Permuta_Aluguer/Details/5
        public async Task<Imovel_Venda_Permuta_Aluguer> get(int? id)
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel_Venda_Permuta_Aluguer/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovelVendaPermutaAluguer = JsonConvert.DeserializeObject<Imovel_Venda_Permuta_Aluguer>(content);

                return imovelVendaPermutaAluguer;
            }
            else
            {
                return null;
            }
        }

        // GET: Imovel_Venda_Permuta_Aluguer/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel_Venda_Permuta_Aluguer/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovelVendaPermutaAluguer = JsonConvert.DeserializeObject<Imovel_Venda_Permuta_Aluguer>(content);
                if (imovelVendaPermutaAluguer == null) return HttpNotFound();
                return View(imovelVendaPermutaAluguer);
            }
            else
            {
                return RedirectToAction("Noauto", "Home");
            }
        }

        // GET: Imovel_Venda_Permuta_Aluguer/Create
        public async Task<ActionResult> Create()
        {
            await PopulateDropDownListTipoImovel(0);
            return View();
        }

        // POST: Imovel_Venda_Permuta_Aluguer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID, TipoImovelID, area, local, latitude, longitude")] Imovel_Venda_Permuta_Aluguer imovelVendaPermutaAluguer, HttpPostedFileBase upload)
        {
            try
            {

                if (upload != null && upload.ContentLength > 0)
                {
                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    {
                        imovelVendaPermutaAluguer.fotoContent = System.Text.Encoding.Default.GetString(reader.ReadBytes(upload.ContentLength));
                    }
                    
                    imovelVendaPermutaAluguer.fotoContentType = upload.ContentType;
                }

                var client = WebApiHttpClient.GetClient();
                string imovelVendaPermutaAluguerJSON = JsonConvert.SerializeObject(imovelVendaPermutaAluguer);
                HttpContent content = new StringContent(imovelVendaPermutaAluguerJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Imovel_Venda_Permuta_Aluguer", content);
                 if (response.IsSuccessStatusCode)
                  {
                      return RedirectToAction("Index");
                  }
                  else
                  {
                return RedirectToAction("Noauto", "Home");
                  }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Imovel_Venda_Permuta_Aluguer/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel_Venda_Permuta_Aluguer/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovelVendaPermutaAluguer = JsonConvert.DeserializeObject<Imovel_Venda_Permuta_Aluguer>(content);
                if (imovelVendaPermutaAluguer == null) return HttpNotFound();

                await PopulateDropDownListTipoImovel(imovelVendaPermutaAluguer.tipoImovelID);
                imovelVendaPermutaAluguer.fotoContent = "";
                return View(imovelVendaPermutaAluguer);
            }
            return RedirectToAction("Noauto", "Home");
        }

        // POST: Imovel_Venda_Permuta_Aluguer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID, TipoImovelID, area, local, latitude, longitude")] Imovel_Venda_Permuta_Aluguer imovelVendaPermutaAluguer, HttpPostedFileBase upload)
        {
            try
            {

                if (upload != null && upload.ContentLength > 0)
                {
                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    {
                        imovelVendaPermutaAluguer.fotoContent = System.Text.Encoding.Default.GetString(reader.ReadBytes(upload.ContentLength));
                    }

                    imovelVendaPermutaAluguer.fotoContentType = upload.ContentType;
                }
                 
                var client = WebApiHttpClient.GetClient();
                string imovelVendaPermutaAluguerJSON = JsonConvert.SerializeObject(imovelVendaPermutaAluguer);
                HttpContent content = new StringContent(imovelVendaPermutaAluguerJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response =
                await client.PutAsync("api/Imovel_Venda_Permuta_Aluguer/" + imovelVendaPermutaAluguer.ID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Noauto", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Imovel_Venda_Permuta_Aluguer/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel_Venda_Permuta_Aluguer/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovelVendaPermutaAluguer = JsonConvert.DeserializeObject<Imovel_Venda_Permuta_Aluguer>(content);
                if (imovelVendaPermutaAluguer == null) return HttpNotFound();
                return View(imovelVendaPermutaAluguer);
            }
            return RedirectToAction("Noauto", "Home");
        }

        // POST: Imovel_Venda_Permuta_Aluguer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Imovel_Venda_Permuta_Aluguer/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Noauto", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        private async Task<Object> PopulateDropDownListTipoImovel(int? ID = null)
        {
            TipoImovelsController ctrl = new TipoImovelsController();
            IEnumerable<TipoImovel> list = await ctrl.getAll();
            TipoImovel TipoImovelID;
            if(ID.Value==0)
            {
                TipoImovelID = null;
            } else
            {
                 TipoImovelID = await ctrl.getTipoImovelbyID(ID.Value);
            }

            if (list == null)
            {
                list = new List<TipoImovel>();
            }

            if (TipoImovelID == null)
            {
                return ViewBag.TipoImovelID = new SelectList(list, "ID", "nome", TipoImovelID);
            }
            else
            {
                return ViewBag.TipoImovelID = new SelectList(list, "ID", "nome", TipoImovelID.ID);
            }

        }
    }
}
