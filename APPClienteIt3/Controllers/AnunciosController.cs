﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APPClienteIt3.Models;
using APPClienteIt3.Helpers;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace APPClienteIt3.Controllers
{
    public class AnunciosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Anuncios
        public async Task<ActionResult> Index()
        {
            ApplicationUserController ctrlUser = new ApplicationUserController();
            ViewBag.User = await ctrlUser.Index();

            if (WebApiHttpClient.getToken() == null)
            {
                return RedirectToAction("Noauto", "Home");
            }

            Anuncio_AluguerController ctrl = new Anuncio_AluguerController();
            Imovel_Venda_Permuta_AluguerController ctrlImovel = new Imovel_Venda_Permuta_AluguerController();
            IEnumerable<Anuncio_Aluguer> list = await ctrl.getAll();
            List<Imovel_Venda_Permuta_Aluguer> listTmp;
            if (list != null)
            {
                ViewBag.AnuncioAluguer = list;
                listTmp = new List<Imovel_Venda_Permuta_Aluguer>();
                foreach(Anuncio_Aluguer a in list)
                {
                    Imovel_Venda_Permuta_Aluguer i = await ctrlImovel.get(a.ImovelID);
                    if(i!=null)
                    {
                        listTmp.Add(await ctrlImovel.get(a.ImovelID));
                    }
                }
                ViewBag.ImovelAluguer = listTmp;
            }
            else
            {
                ViewBag.AnuncioAluguer = new List<Anuncio_Aluguer>();
            }

            Anuncio_CompraController ctrl1 = new Anuncio_CompraController();
            IEnumerable<Anuncio_Compra> list1 = await ctrl1.getAll();
            if (list1 != null)
            {
                ViewBag.AnuncioCompra = list1;

            }
            else
            {
                ViewBag.AnuncioCompra = new List<Anuncio_Compra>();
            }
           
           
            Anuncio_PermutaController ctrl2 = new Anuncio_PermutaController();
            IEnumerable<Anuncio_Permuta> list2 = await ctrl2.getAll();
            if (list2 != null)
            {
                ViewBag.AnuncioPermuta = list2;
                listTmp = new List<Imovel_Venda_Permuta_Aluguer>();
                foreach (Anuncio_Permuta a in list2)
                {
                    Imovel_Venda_Permuta_Aluguer i = await ctrlImovel.get(a.ImovelID);
                    if (i != null)
                    {
                        listTmp.Add(await ctrlImovel.get(a.ImovelID));
                    }
                }
                ViewBag.ImovelPermuta = listTmp;
            }
            else
            {
                ViewBag.AnuncioPermuta = new List<Anuncio_Permuta>();
            }
            
            Anuncio_VendaController ctrl3 = new Anuncio_VendaController();
            IEnumerable<Anuncio_Venda> list3 = await ctrl3.getAll();
            if (list3 != null)
            {
                ViewBag.AnuncioVenda = list3;
                listTmp = new List<Imovel_Venda_Permuta_Aluguer>();
                foreach (Anuncio_Venda a in list3)
                {
                    Imovel_Venda_Permuta_Aluguer i = await ctrlImovel.get(a.ImovelID);
                    if (i != null)
                    {
                        listTmp.Add(await ctrlImovel.get(a.ImovelID));
                    }
                }
                ViewBag.ImovelVenda = listTmp;
            }
            else
            {
                ViewBag.AnuncioVenda = new List<Anuncio_Venda>();
            }
            
            return View();
        }

    }
}
