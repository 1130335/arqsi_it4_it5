﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APPClienteIt3.Models;
using APPClienteIt3.Helpers;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
namespace APPClienteIt3.Controllers
{
    public class FotoController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public async Task<ActionResult> Index(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Fotoes/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var fileToRetrieve = JsonConvert.DeserializeObject<Foto>(content);
                if (fileToRetrieve == null) return HttpNotFound();

                return File(System.Text.Encoding.Default.GetBytes(fileToRetrieve.Content), fileToRetrieve.ContentType);
            }
            else
            {
                return RedirectToAction("Erro", "Home");
            }
        }
    }
}
