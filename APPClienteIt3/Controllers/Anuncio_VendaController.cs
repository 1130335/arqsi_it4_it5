﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APPClienteIt3.Models;
using APPClienteIt3.Helpers;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace APPClienteIt3.Controllers
{
    public class Anuncio_VendaController : Controller
    {

        // GET: Anuncio_Venda
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Venda/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncioVenda =
                JsonConvert.DeserializeObject<IEnumerable<Anuncio_Venda>>(content);
                List<Imovel_Venda_Permuta_Aluguer> list = new List<Imovel_Venda_Permuta_Aluguer>();
                Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
                foreach (Anuncio_Venda a in anuncioVenda)
                {
                    list.Add(await ctrl.get(a.ImovelID));
                }
                ViewBag.Imoveis = list;
                return View(anuncioVenda);
            }
            else
            {
                return RedirectToAction("Noauto", "Home");
            }
        }

        public async Task<IEnumerable<Anuncio_Venda>> getAll()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Venda/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anunciosVenda =
                JsonConvert.DeserializeObject<IEnumerable<Anuncio_Venda>>(content);
                return anunciosVenda;
            }
            else
            {
                return null;
            }
        }

        // GET: Anuncio_Venda/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Venda/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncioVenda = JsonConvert.DeserializeObject<Anuncio_Venda>(content);
                if (anuncioVenda == null) return HttpNotFound();
                Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
                @ViewBag.Imovel = await ctrl.get(anuncioVenda.ImovelID);
                return View(anuncioVenda);
            }
            else
            {
                return RedirectToAction("Noauto", "Home");
            }
        }

        // GET: Anuncio_Venda/Create
        public async Task<ActionResult> Create()
        {
            await PopulateDropDownListImovel(0);
            Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
            IEnumerable<Imovel_Venda_Permuta_Aluguer> list = await ctrl.getAll();
            if (list == null)
            {
                list = new List<Imovel_Venda_Permuta_Aluguer>();
            }
            ViewBag.Imovel = list;
            return View();
        }

        // POST: Anuncio_Venda/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,preco")] Anuncio_Venda anuncioVenda, int Imovel)
        {

            try
            {
                anuncioVenda.ImovelID = Imovel;
                var client = WebApiHttpClient.GetClient();
                string anuncioVendaJSON = JsonConvert.SerializeObject(anuncioVenda);
                HttpContent content = new StringContent(anuncioVendaJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Anuncio_Venda", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index", "Anuncios");
                }
                else
                {
                    return RedirectToAction("Erro", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Anuncio_Venda/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Venda/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncioVenda = JsonConvert.DeserializeObject<Anuncio_Venda>(content);
                if (anuncioVenda == null) return HttpNotFound();

                await PopulateDropDownListImovel(anuncioVenda.ImovelID);
                Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
                IEnumerable<Imovel_Venda_Permuta_Aluguer> list = await ctrl.getAll();
                if (list == null)
                {
                    list = new List<Imovel_Venda_Permuta_Aluguer>();
                }
                ViewBag.Imovel = list;
                return View(anuncioVenda);
            }
            return RedirectToAction("Noauto", "Home");
        }
        
        // POST: Anuncio_Venda/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,preco")] Anuncio_Venda anuncioVenda, int Imovel)
        {
            try
            {
                anuncioVenda.ImovelID = Imovel;
                var client = WebApiHttpClient.GetClient();
                string anuncioVendaJSON = JsonConvert.SerializeObject(anuncioVenda);
                HttpContent content = new StringContent(anuncioVendaJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response =
                await client.PutAsync("api/Anuncio_Venda/" + anuncioVenda.ID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index", "Anuncios");
                }
                else
                {
                    return RedirectToAction("Erro", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Anuncio_Venda/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Venda/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncioVenda = JsonConvert.DeserializeObject<Anuncio_Venda>(content);
                if (anuncioVenda == null) return HttpNotFound();
                Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
                @ViewBag.Imovel = await ctrl.get(anuncioVenda.ImovelID);
                return View(anuncioVenda);
            }
            return RedirectToAction("Noauto", "Home");
        }

        // POST: Anuncio_Venda/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Anuncio_Venda/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index", "Anuncios");
                }
                else
                {
                    return RedirectToAction("Noauto", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        private async Task<Object> PopulateDropDownListImovel(object ImovelID = null)
        {
            Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
            IEnumerable<Imovel_Venda_Permuta_Aluguer> list = await ctrl.getAll();
            if (list == null)
            {
                list = new List<Imovel_Venda_Permuta_Aluguer>();
            }
            return ViewBag.ImovelID = new SelectList(list, "ID", "ID", ImovelID);

        }
    }
}
