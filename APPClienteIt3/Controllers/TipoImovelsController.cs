﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APPClienteIt3.Models;
using APPClienteIt3.Helpers;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;


namespace APPClienteIt3.Controllers
{
    public class TipoImovelsController : Controller
    {
        //private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TipoImovels
        public async Task<ActionResult> Index()
        {
            RoleController r = new RoleController();
            List<string> l = await r.Index();
            ViewBag.Role = l.ElementAt(0);

            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovels/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tipoImovels =
                JsonConvert.DeserializeObject<IEnumerable<TipoImovel>>(content);
                return View(tipoImovels);
            }
            else
            {
                return RedirectToAction("Noauto", "Home");
            }
        }
        
        public async Task<IEnumerable<TipoImovel>> getAll()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovels/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tipoImovels =
                JsonConvert.DeserializeObject<IEnumerable<TipoImovel>>(content);
                return tipoImovels;
            }
            return null;
        }

        public async Task<TipoImovel> getTipoImovelbyID(int id)
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovels/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tipoImovels =
                JsonConvert.DeserializeObject<IEnumerable<TipoImovel>>(content);
                foreach (TipoImovel tmp in tipoImovels)
                {
                    if (tmp.ID == id)
                    {
                        return tmp;
                    }
                }
            }
            return null;
        }

        // GET: TipoImovels/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovels/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tipoImovel = JsonConvert.DeserializeObject<TipoImovel>(content);
                if (tipoImovel == null) return HttpNotFound();
                return View(tipoImovel);
            }
            else
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: TipoImovels/Create
        public async Task<ActionResult> Create()
        {
            await PopulateDropDownListTipoImovel();
            return View();
        }

        // POST: TipoImovels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,nome")] TipoImovel tipoImovel, string TipoImovelID)
        {

            try
            {
                if (TipoImovelID.Length != 0)
                {
                    TipoImovelsController ctrl = new TipoImovelsController();
                    TipoImovel tmp = await ctrl.getTipoImovelbyID(int.Parse(TipoImovelID));
                    
                    tipoImovel.subtipo_id = tmp.ID;
                }

                var client = WebApiHttpClient.GetClient();
                tipoImovel.subtipo = "";
                string tipoImovelJSON = JsonConvert.SerializeObject(tipoImovel);
                HttpContent content = new StringContent(tipoImovelJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/TipoImovels", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Noauto", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        public ActionResult DeleteNot()
        {
            return View();
        }

        // GET: TipoImovels/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovels/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tipoImovel = JsonConvert.DeserializeObject<TipoImovel>(content);
                if (tipoImovel == null) return HttpNotFound();
                if(tipoImovel.subtipo_id != 0)
                { 
                    await PopulateDropDownListTipoImovel(tipoImovel.subtipo_id);
                } 
                else
                {
                    await PopulateDropDownListTipoImovel();
                }

                return View(tipoImovel);
            }
            return RedirectToAction("Erro", "Home");
        }

        // POST: TipoImovels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,nome")] TipoImovel tipoImovel, string TipoImovelID)
        {

            try
            {
                if (TipoImovelID.Length != 0)
                {
                    TipoImovelsController ctrl = new TipoImovelsController();
                    TipoImovel tmp = await ctrl.getTipoImovelbyID(int.Parse(TipoImovelID));
                    tipoImovel.subtipo_id = tmp.ID;
                }
                else
                {
                    tipoImovel.subtipo_id = 0;
                }

                var client = WebApiHttpClient.GetClient();
                string tipoImovelJSON = JsonConvert.SerializeObject(tipoImovel);
                HttpContent content = new StringContent(tipoImovelJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response =
                await client.PutAsync("api/TipoImovels/" + tipoImovel.ID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Noauto", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: TipoImovels/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovels/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tipoImovel = JsonConvert.DeserializeObject<TipoImovel>(content);
                if (tipoImovel == null) return HttpNotFound();
                return View(tipoImovel);
            }
            return RedirectToAction("Noauto", "Home");
        }

        // POST: TipoImovels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/TipoImovels/" + id);
                if (response.ReasonPhrase == "Forbidden")
                {
                    return RedirectToAction("DeleteNot");
                }
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Noauto", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        private async Task<Object> PopulateDropDownListTipoImovel(object TipoImovelID = null)
        {
            TipoImovelsController ctrl = new TipoImovelsController();
            IEnumerable<TipoImovel> list = await ctrl.getAll();
            if(list==null)
            {
                list = new List<TipoImovel>();
            }
            return ViewBag.TipoImovelID = new SelectList(list, "ID", "nome", TipoImovelID);

        }
    }
}
