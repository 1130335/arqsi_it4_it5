﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using APPClienteIt3.Models;
using APPClienteIt3.Helpers;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using APPClienteIt3.ViewModels;

namespace APPClienteIt3.Controllers
{
    public class VisitasController : Controller
    {

        // GET: Visitas
        public async Task<ActionResult> Index()
        {
            ApplicationUserController ctrlUser = new ApplicationUserController();
            ViewBag.User = await ctrlUser.Index();

            if (WebApiHttpClient.getToken() == null)
            {
                return RedirectToAction("Noauto", "Home");
            }


            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Visitas/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var visitas = JsonConvert.DeserializeObject<IEnumerable<Visita>>(content);
                return View(visitas);
            }
            else
            {
                return RedirectToAction("Noauto", "Home");
            }
        }

        public async Task<IEnumerable<Visita>> getAll()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Visitas/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var visitas =
                JsonConvert.DeserializeObject<IEnumerable<Visita>>(content);
                return visitas;
            }
            return null;
        }

        public async Task<Visita> getVisitabyID(int id)
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Visitas/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var visitas =
                JsonConvert.DeserializeObject<IEnumerable<Visita>>(content);
                foreach (Visita tmp in visitas)
                {
                    if (tmp.ID == id)
                    {
                        return tmp;
                    }
                }
            }
            return null;
        }

        // GET: Visitas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Visitas/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var visitas = JsonConvert.DeserializeObject<Visita>(content);
                if (visitas == null) return HttpNotFound();
                return View(visitas);
            }
            else
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Visitas/Create
        public async Task<ActionResult> Create()
        {
            await PopulateDropDownListVisitas();
            return View();
        }

        // POST: Visitas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,imovelID,tipoImovel,HoraInicio,HoraFim,Dia,Mes,Ano")] Visita visita)
        {

            try
            {

                var client = WebApiHttpClient.GetClient();
                string visitaJSON = JsonConvert.SerializeObject(visita);
                HttpContent content = new StringContent(visitaJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Visitas", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Noauto", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }
        /*
        public ActionResult DeleteNot()
        {
            return View();
        }
        */

        // GET: Visitas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Visitas/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var visita = JsonConvert.DeserializeObject<Visita>(content);
                if (visita == null) return HttpNotFound();

                return View(visita);
            }
            return RedirectToAction("Erro", "Home");
        }

        // POST: Visitas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,imovelID,tipoImovel,HoraInicio,HoraFim,Dia,Mes,Ano")] Visita visita)
        {

            try
            {

                var client = WebApiHttpClient.GetClient();
                string visitaJSON = JsonConvert.SerializeObject(visita);
                HttpContent content = new StringContent(visitaJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response =
                await client.PutAsync("api/Visitas/" + visita.ID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Noauto", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Visitas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Visitas/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var visita = JsonConvert.DeserializeObject<Visita>(content);
                if (visita == null) return HttpNotFound();
                return View(visita);
            }
            return RedirectToAction("Noauto", "Home");
        }

        // POST: Visitas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Visitas/" + id);
                /*if (response.ReasonPhrase == "Forbidden")
                {
                    return RedirectToAction("DeleteNot");
                }*/
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Noauto", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        private async Task<Object> PopulateDropDownListVisitas(object VisitaID = null)
        {
            VisitasController ctrl = new VisitasController();
            IEnumerable<Visita> list = await ctrl.getAll();
            if (list == null)
            {
                list = new List<Visita>();
            }
            return ViewBag.VisitaID = new SelectList(list, "ID", "imovelID", VisitaID);

        }
    }
}
